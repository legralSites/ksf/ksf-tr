#!/bin/sh
# Ce fichier doit etre copier dans le repertoire ./ (racine) du projet et modifier

# - configuration - #


localConcat (){
        # - concatenation des fichiers css - #
        echo "$couleurINFO # - concatenation des fichiers css dans ./styles/styles.css - #$couleurNORMAL";
	if [ $isSimulation -eq 0 ];then
	        catCSS /www/sites/intersites/lib/tiers/css/knacss/knacss.css
	        #catCSS /www/git/intersites/lib/legral/php/gestLib/versions/gestLib-0.1.css
		catCSS ./styles/intersites.css
		catCSS ./styles/html4.css 

		#catCSS ./styles/gestLib.css
		catCSS /www/git/intersites/lib/legral/php/gestLib/styles/gestLib.css
		catJS /www/git/intersites/lib/legral/php/gestLib/gestLib.js
		catJS ./lib/tiers/js/jscolor/jscolor-1.4.3/jscolor.js

		catCSS /www/git/intersites/lib/legral/js/gestClasseurs/styles/gestClasseurs.css
		catJS /www/git/intersites/lib/legral/js/gestClasseurs/gestClasseurs.js
		

	#	catCSS ./styles/ksf.css
	#	catCSS ./styles/html4-local.css
	#	catCSS ./lib/legral/php/gestLib/gestLib.css
	#	catCSS ./styles/notes/notes.css
	#	catCSS ./styles/tutoriels/tutoriels-style.css
	#	catCSS ./lib/legral/php/menuStylisee/styles/menuOnglets-defaut/menu.css
	fi
        # - concatenation des fichiers js - #
        echo "$couleurINFO # - concatenation des fichiers js dans ./locales/scripts.js - #$couleurNORMAL";
	if [ $isSimulation -eq 0 ];then
		catJS  ./locales/void.js
		#catJS /www/git/intersites/lib/tiers/js/jquery/jquery-2.1.1.min.js
		#catJS /www/git/intersites/lib/legral/php/gestLib/gestLib.js
		#catJS ./locales/consoleBox.js
		#catJS ./locales/ioAnimate.js
	fi
	}
##################################################
# localSave()                                    #
# copie un fichier en le suffixant de la version #
##################################################
localSave() {
	echo "$couleurINFO versionning des fichiers locaux$couleurNORMAL";
	#if [ $isSimulation -eq 0 ];then

		# - fichier a copier dans versions (en ajoutant la version dans le mon du fichier) - #
		#versionSave ./scripts/gitVersion sh
		#versionSave ./localVersion sh

		#exemple:
		#versionSave script js
	#fi
	}
#######################################
# localStatification()                #
# télécharge et rend statique un site #
#######################################
localStatification(){
	echo "$couleurINFO # - localVersion.sh:localStatification() - #$couleurNORMAL";
	url="http://127.0.0.1/git/legralDocs/legralNet/";

	echo "telechargement avec rendu statique d'un site de $url";

	if [ $isSimulation -eq 0 ];then
		#dirname=`dirname $0`;		#dirname=${0##*/};		echo "dirname: ${dirname}";
		#gitVersion a mit le rep racine du projet "../scripts" comme rep de travail
		#echo "$couleurINFO repertoire de travail$couleurNORMAL";pwd;

		echo "$couleurINFO suppression et creation de ../statique/$couleurNORMAL";
		rm -R ./statique/;mkdir ./statique/;cd   ./statique/;

		echo "$couleurINFO téléchargement...$couleurWARN";

		#--no-verbose --quiet
		wget --quiet --tries=5 --continue --no-host-directories --html-extension --recursive --level=inf --convert-links --page-requisites --no-parent --restrict-file-names=windows --random-wait --no-check-certificate $url

		echo "$couleurINFO deplacer et nettoyer le chemin$couleurNORMAL";
		mv ./git/legralDocs/legralNet ./

		# - decommenter pour activer la suppression apres verification - #
		rm -R ./git/
	fi
}

#######################################
# syncRemote()                        #
# synchronise le(s) serveurs distants #
#######################################
syncRemote(){
	echo "$couleurINFO # - localVersion.sh:syncRemote() - #$couleurNORMAL";
	if [ $isSimulation -eq 0 ];then
		# - http://doc.ubuntu-fr.org/lftp - #
		# configurer ~.netrc pour ne pas a avoir a mettre le passsword

		# repertoire local ./repereTemporel sera (ftp.legral.fr/ /rt/repereTemporel
		if [ $isSimulation -eq 0 ];then
			#exemple:
			#lftp -u legral ftp://ftp.legral.fr -e "mirror -e -R  ./statique/vampire   /dijonraxis0/ ; quit"
			 lftp -u legral ftp://ftp.legral.fr -e "mirror -e -R  ./statique/   /rt/ ; quit"
		fi
	fi
}

#################
# postGit()     #
# lancer en fin #
#################
postGit() {
	echo "$couleurINFO # - localVersion.sh:postGit() - #$couleurNORMAL";
	# if [ $isSimulation -eq 0 ];then
		#
	# fi
	}
