/***************************************
 * animation
 ***************************************/
var ioAnimate=null; // preparation a l'instanciation

function TioAnimate(){
	this.idJS=this;
	this.delay=500;

	this.setIdCSS=function(ioUpHTMLId,ioDwHTMLId){
		this.ioUpIdCSS=document.getElementById(ioUpHTMLId);
		this.ioDwIdCSS=document.getElementById(ioDwHTMLId);
	}

	this.ioUpShow=function(cb_post){
		this.ioUpIdCSS.style.visibility="visible";
		setTimeout(function(){ioAnimate.ioUpIdCSS.style.visibility="hidden";
			if(typeof(cb_post)=="function")cb_post();
		},this.delay);
	}
	this.ioDwShow=function(cb_post){
		this.ioDwIdCSS.style.visibility="visible";
		setTimeout(function(){ioAnimate.ioDwIdCSS.style.visibility="hidden";if(typeof(cb_post)=="function")cb_post();},this.delay);
	}
	this.check=function(){
		this.ioUpShow(ioAnimate.ioDwShow);
	//	this.ioUpShow();
	//	this.ioDwShow();

	}

	return this;
}

