/***************************************
 * loadRefuges () )
 * charge tous les refuges d'un personnage
 * cas de la gestion multipersonnage?
 * ***************************************/	
function loadRefuges(){
	var prefixe="loadRefuges(): ";
	gestLib.write({"lib":"socketio","ALL":prefixe+"BEGIN"});
	var plis= new TgestPlis();
	plis.cmd='refugesGet';
	plis.cmdReponse='refugesSet';

	// - charger le(s) refuges de chaque personnage - //
	for(var p in gestPersonnages.personnages){
		if(p==undefined)continue;

		var uuid=gestPersonnages.personnages[p].datas.uuid;
		plis.p_uuid=uuid;	// uuid du personnage selectionne
		plis.sqlProcNom="refugesGet";

		gestLib.write({"lib":"socketio","ALL":plis.cmd+": chargement des refuges de "+gestPersonnages.personnages[p].datas.nom});
		gestSocket.socket.emit(plis.cmd,plis);
	}
	gestLib.write({"lib":"socketio","ALL":prefixe+plis.cmd+": END"});
}

function refugesSet(gestSocket){
	// - ---------------------------------------- - -//
	// - reception des refuges - //
	gestSocket.socket.on('refugesSet', function (plis) {
		var prefixe=plis.cmdReponse+": ";
		var plis=JSON.parse(plis);
		gestLib.write({"lib":"socketio","ALL":plis.cmdReponse+": BEGIN"});

//		if(plis_isErreur(plis))return null;

		if (plis.sqlRows==undefined){
			gestLib.write({"lib":"socketio","ALL":prefixe+plis.cmdReponse+": reponse sql: aucune"});
		}
		else{
			var liste=plis.sqlRows[0];
			gestRefuges.add(liste);

			// - on charge la  page fiche-spheres - //
			plis= new TgestPlis({
				"page":"fiche-refuges"					// chargement de la page "fiche-personnage"
				,"feuilleDest":"fiche-refuges"				// la page sera afficher dans cette feuille
			});	

			// - chargement de la page et affichage dans la feuille - //
			getPage(plis);
		}
		gestLib.write({"lib":"socketio","ALL":prefixe+plis.cmdReponse+":END"});
	});
} // refugesSet()
