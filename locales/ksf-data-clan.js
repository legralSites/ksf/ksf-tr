function clan_data(){
	this.id=0;
	this.nom="";
	this.description="";
	return this;
}

function gestClan(init){
	this.datas=new clan_data();
	this.setDatas(init);
	return this;
}

gestclan.prototype={
	setDatas:function(dt){
		if(typeof(dt)!="object")return null;
		if(dt.id)		this.setId(dt.id);

		if(dt.nom)		this.setNom(dt.nom);
		if(dt.description)	this.setDescription(dt.description);
	return null;
	}
	,setId:function(v)		{if(!isNaN(v))this.datas.id=v;				return null;}
	,setNom:function(v)		{if(v!="")this.datas.nom=v;				return null;}
	,setDescription:function(v)	{if(v!="")this.datas.description=v;			return null;}

}// gestClan.prototype=


function gestClans(){
	this.clans=array();
	return this;
}
gestclans.prototype={
	// - ajoute un object clan au tableau - //
	clanAdd:function(data){
		this.clans[this.clan.length)]=new gestClan(data);


	}
}


// -- activer en tant que module node.js -- //
try{
//	exports.gestJoueur=new gestJoueurs();	// gestionnaire deS joueurS
//	exports.joueur=new joueur();		// structure de donneed pour 1 joueur
//	exports.personnage=new personnage();
	exports.gestClan=new gestClan();
//	exports.=new ();
//	exports.=new ();
	}catch(e){}

