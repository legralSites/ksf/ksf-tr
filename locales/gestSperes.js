function Tsphere_data(){
	this.id=0
	this.nom='';
	this.description="";
	return this;
}

function TgestSphere(c){
	this.datas=new Tsphere_data();
	this.setDatas(c);
	return this;
}

TgestSphere.prototype={
	setDatas:function(dt){
		if(typeof(dt)!="object")return null;
		if(dt.s_id) this.setId(dt.s_id);
		if(dt.s_nom)this.datas.nom=dt.s_nom;
		if(dt.s_description)this.datas.description=dt.s_description;
	return null;
	}
	,setId:function(v) {if(!isNaN(v))this.datas.id=v;	return null;}

	/******************************
	 * fonction pour les clients
	 * le client ne gere qu'un joueur
	 * et gere l'affichage
	 *****************************/
	,show:function(){
		//document.getElementById('s_id').innerHTML=this.datas.j_id;
		//document.getElementById('s_nom').innerHTML=this.datas.j_nom;
		//document.getElementById('s_prenom').innerHTML=this.datas.j_prenom;
	}

}// TgestSphere.prototype=

/******************************
 * fonction pour le serveur node
 * node appelle gestJoueurs
 * pour gerer et mettre en cache les joueurs 
 *****************************/
function TgestSpheres(){
	this.nb=0;
	this.spheres=Array();
	return this;
}


TgestSpheres.prototype={

	// - ajoute un (seul) personnage p est un objet (et non un array)  - // 
	_add:function(c){
		var erreur=0;
		this.nb++;
		this.spheres[c.id]= new TgestSphere(c);

		// - erreur a definir: abs de nom? etc - //
		if(erreur!=0){
			this.spheres[c.id]=null;
			this.nb--;
		}
		return null;
	}

	// - ajoute une (objet) ou plusieurs (tableau d'objet) sphere(s) - //
	,add:function(liste){
		// - est ce un tableau d'object? - //
		if(typeof(liste[0].id)=='number')
			for (var p in liste) this._add(liste[p]);
		// cet object ne contient pas de variable nomme 0
		else this._add(liste);
	}
	,getNomById:function(id){
		return this.spheres[id].datas.nom;
	}

	// --- fonctions specifiques au client --- //

	,showAll:function(IDHTML){
		if(!IDHTML)IDHTML='spheres-liste';
		var IDCSS=document.getElementById(IDHTML);
		if(IDCSS==undefined){
			return null;
		}

		IDCSS.innerHTML="";

		for(var sphere in this.spheres){
			if(sphere==undefined)continue;
			var c=this.spheres[sphere].datas;
			if(c==undefined)continue;
			IDCSS.innerHTML+="<div>"+c.nom+"</div>";
			IDCSS.innerHTML+="<div>"+c.description+"</div>";
		}
	}
}




// -- activer en tant que module node.js -- //
try{
	exports.TgestSphere=TgestSphere;
	exports.gestSphere= new TgestSphere();

	exports.TgestSpheres=TgestSpheres;
	exports.gestSpheres= new TgestSpheres();
	}catch(e){}


