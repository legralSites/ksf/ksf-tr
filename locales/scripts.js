

/*! - /www/git/intersites/lib/legral/php/gestLib/gestLib.js - */
/*!
fichier: gestLib.js
auteur:pascal TOLEDo
date de creation : 2012.02.21
date d emodification: 2014.08.08
source: http://gitorious.org/gestLib
*/

GESTLIBVERSION= '2.0.0';

// gestionnaire de la box des donnees
function gestionLibrairie_dataBox(o){
//	this.nom=o.nom;		//facultatif
	this.idHTML=o.idHTML;	//facultatif
	this.idCSS=null;
	this.idCSSParent=o.idCSSParent;	// obligatoire
	this.className='gestLibDataBox';
	if(o.className)this.className+=' '+o.className;

	this.genre=(o.genre=='TEXTAREA')?'TEXTAREA':'DIV';	// 'TEXTAREA' sinon DIV par defaut
	return this;
}

gestionLibrairie_dataBox.prototype={
	create:function(){
		this.idCSS=document.createElement(this.genre.toLowerCase());
		this.idCSSParent.appendChild(this.idCSS);
		this.init();
		return this.idCSS;
	}
	// - 're)initialisation du contenaire - //
	,init:function(){
		this.idCSS.id=this.idHTML;
		this.idCSS.className=this.className;
		this.idCSS.style.display='block';
		return null;
	}

	// o: object
	// o.ALL: texte qui sera ecrit quelque soit le genre de dataBox
	// o.DIV: texte ecrit uniquement si dataBox.genre == DIV
	// o.TEXTAREA: texte ecrit uniquement si dataBox.genre == TEXTAREA
	// o.notWrite: n'ecrit pas dans la console mais retroune seulement le texte formater

	,write:function(o){
//		if(typeof(o)!='object)')return null;
		var str=o;	// par defaut o est supposé un string
		if(o.ALL)str=o.ALL;	// surcharge si toute destination preciser
		var br='';
		var isObj=(typeof(o)=='object');
		if(this.genre=='TEXTAREA'){
			if(!o.ALL && !o.TEXTAREA){return '';}
			if(isObj && o.TEXTAREA)str=o.TEXTAREA;	// surcharge si specifier
			br=(o.br!=undefined)?o.br:'\n';
			str+=br;
			if(!o.notWrite)this.idCSS.value+=str;
		}
		if(this.genre=='DIV'){
			if(!o.ALL && !o.DIV){return '';}
			if(isObj && o.DIV)str=o.DIV;	// surcharge si specifier
			br=(o.br!=undefined)?o.br:'<br>';
			str+=br;
			if(!o.notWrite)this.idCSS.innerHTML+=str;
		}
		return str;
	}


}


//options: nom:obligatoire
//erreurNo:
// -1: absence de nom
// -10..-11..-12: absence de div pour la console
// 100+ specifique a la lib appellé
function gestionLibrairie_lib(options)
	{
	if((options.nom==undefined)&&((options.idHTML==undefined)))return null;	// l'un ou l'autre doit etre definit
	this.erreurNo=0;
	this.erreurTxt=new Array();//attention tableau de tableau
	if (options.nom==undefined){this.erreurNo=-1;}
	this.date=new Date();
	this.libType=(options.libType=='tiers')?'tiers':'perso';	// defaut:pas de console
	this.isConsole=(options.isConsole===1)?1:0;	// defaut:pas de console
	this.consoleGenre=null;	// TEXTAREA,DIV
	this.isVisible=(options.isVisible===0)?0:1;	// defaut:visible
	this.nom=   (options.nom   !=undefined)?options.nom   :options.idHTML;

	// - console - //
	this.idCSSSup=null;	this.idCSSNom=null;	this.idCSSTxt=null;
	this.idHTML=(options.idHTML!=undefined)?options.idHTML:options.nom;
	this.idHTMLMenus=this.idHTML+'_menus';
	this.idCSSMenus=null;	// support des menus
	this.dataBox=null;

	this.description=(options.description!=undefined)?options.description:'';
	this.ver=(options.ver!=undefined)?options.ver:0;
	this.url=(options.url!=undefined)?options.url:null;//site du code source
	this.deb=this.date.getTime();
	this.fin=null;	this.dur=null;
	if (this.isConsole)	{this.setConsole();}

	this.instances=new Array();//liste des instances de la lib

	this.erreurTranslate();//en fin car neccesite variable

	return this; //renvoie un pointeur
	}

gestionLibrairie_lib.prototype=
	{
	erreurTranslate:function()
		{
		this.erreurTxt['fr']=new Array();
		this.erreurTxt['fr'][0]='ok';
		this.erreurTxt['fr'][1]='non charger';
		this.erreurTxt['fr'][-1]='nom non defini';
		this.erreurTxt['fr'][-10]='div '+this.idHTML+'_Support non defini';
		this.erreurTxt['fr'][-11]='span '+this.idHTML+'_Nom non defini';
		this.erreurTxt['fr'][-12]='div/textarea '+this.idHTML+'_Texte non defini';
		}
	,erreurShow:function(instNu,errNu,lang)
		{
		return this.erreurTxt[lang?lang:'fr'][errNu?errNu:this.erreurNo];
		}

	,destruct:function(){}
	,end:function(){this.date=new Date();this.fin=this.date.getTime();this.dur=this.fin-this.deb;}
	
	// ajoute le param ds la liste si object et renvoie 1 sinon eznvoie 0
	,instanceAdd:function(instPtr)
		{
		if(typeof(instPtr)==='object'){this.instances[this.instances.length]=instPtr;return 1;}
		return 0;
		}
	,ongletAdd:function(f){
		if(typeof(f)!='object')return null;
	
		// --  creation de la div this.creerCSS(); -- //
		var idCSS=document.createElement('span');
		this.idCSSMenus.appendChild(idCSS);

		// ==  initilialisation de creation == //
		idCSS.id=f.idHTML;
		idCSS.className="gestLibNom gestLibOnglet";
		idCSS.innerHTML=f.texte;
		//		idCSS.style.position='absolute';
//		idCSS.style.overflow='hidden';
//		idCSS.style.top=0;
//		idCSS.style.zIndex=this.zIndex;
//		idCSS.style.height =this.hauteur+'px';
		idCSS.style.cursor='pointer';
//		idCSS.style.opacity=this.opacity;
		return idCSS;
	}

	,setConsole:function(genre){
		// - support - //
		this.idCSSSup=document.getElementById(this.idHTML+'Console');	if (this.idCSSSup==null){this.erreurNo=-10;return null;}
//		this.consoleGenre=this.idCSSSup.tagName.toUpperCase(); // DIV, TEXTAREA
//		g=(typeof(genre)=='string')?genre.toUpperCase():'DIV';
	//	this.consoleGenre=(typeof(genre)!=)genre.toUpperCase();

		// - creation du support des menus - //
		this.idCSSMenus=document.createElement('div');
		this.idCSSMenus.id=this.idHTMLMenus;	// utile?
		this.idCSSSup.appendChild(this.idCSSMenus); 

		// -- onglet:nom(show|hide) -- //
		var f={"idHTML":this.idHTML+"_nom","texte":"console:"+this.nom};
		this.idCSSNom=this.ongletAdd(f);
		this.idCSSNom.onclick=function(elt){
			var CSSId=elt.currentTarget.parentElement.parentElement.children[1].style; // attention ordre de l'elt code en DUR!!!
			CSSId.display=(CSSId.display=='block')?'none':'block';
		}

		// -- onglet:clear(efface le contenu) -- //
		var f={"idHTML":this.idHTML+"_clear","texte":"clear"};
		this.idCSSClr=this.ongletAdd(f);
		this.idCSSClr.onclick=function(elt){
			var CSSId=elt.currentTarget.parentElement.parentElement.children[1]; // atention ordre de l'elt code en DUR!!!
			switch (CSSId.tagName.toUpperCase()){ 
				case 'TEXTAREA':CSSId.value="";break;
				case 'DIV':default:CSSId.innerHTML="";break;
			}
		}

		// - creation de la dataBox - //
		this.dataBox=new gestionLibrairie_dataBox({"idCSSParent":this.idCSSSup,"genre":genre});
		this.dataBox.create();
		//		this.idCSSTxt=document.getElementById(this.idHTML+'Texte');	if (this.idCSSTxt==null){this.erreurNo=-12;}


		// -- onglet:eval(si genre TEXTAREA) : neccessite que la dataBox soit creer! -- //
		if(this.dataBox.genre=='TEXTAREA'){
			var f={"idHTML":this.idHTML+"_eval","texte":"eval"};
			this.idCSSEval=this.ongletAdd(f);
			this.idCSSEval.onclick=function(elt){
				var data=elt.currentTarget.parentElement.parentElement.children[1].value; // attention ordre de l'elt code en DUR!!!
				eval(data);
				}
		}

		if (!this.erreurNo){this.idCSSNom.innerHTML='console:'+this.nom;}
		this.dataBox.write("console activée");

		//	if (!this.isVisible){this.hide();}
		this.isConsole=1;
	}

	,valueToString:function(val){
		switch (typeof(val)){
			case 'undefined':return 'undefined';
			case null: return 'null';
		}
		return val;
	}

	,evaluer:function()
		{
		if (this.idCSSTxt)
			{switch (this.idCSSTxt.tagName.toUpperCase())
				{
				case 'TEXTAREA':eval(this.idCSSTxt.value);break;
				case 'DIV':default:eval(this.idCSSTxt.innerHTML);
				break;
				}
			}
		}
	,write:function(o){this.dataBox.write(o);}
	
	// - inspect renvoie un texte avec le nom et la valeur d'une variable DANS la console - //
	,inspect:function(o){
		if(typeof(o)!='object')return null;
		var toV=typeof(o.varPtr);
		return this.dataBox.write({
//			"notWrite":1
			"TEXTAREA":'['+toV+'] '+o.varNom+'='+o.varPtr
			,"DIV":'<span class="gestLibVarNom">'+'['+toV+'] '+o.varNom+'</span>=<span class="gestLibVar">'+this.valueToString(o.varPtr)+'</span>'
		});
	}

	,inspectAll:function(o){
		var out='';
		o.notWrite=1;

		switch (typeof(o.varPtr)){
			case 'object':
//				if(this.dataBox.genre=='DIV')out+='<span class="gestLibVal">';
				// - affichage du nom de la variable - //
				if (o.varNom){
					out+=this.dataBox.write({
						"notWrite":1
						,"TEXTAREA":o.varNom+'='+o.varPtr
						//,"DIV":'<span class="gestLibVarNom">'+o.varNom+'</span>=<span class="gestLibVar">'+this.valueToString(o.varPtr)+'</span>'
						,"DIV":'<span class="gestLibVarNom">'+o.varNom+'</span>'
					});
				}

				if(this.dataBox.genre=='DIV')out+='<ul>';
				for (var v in o.varPtr){
					var to=typeof(o.varPtr[v]);
					out+='--'+to+'--';
					//if(typeof(v)==undefined)continu0e;
					if(this.dataBox.genre=='DIV')out+='<li>'; else out+=' * ';
					out+=this.inspectAll({"lib":o.lib,"varNom":v,"varPtr":o.varPtr[v],"notWrite":1});
					if(this.dataBox.genre=='DIV')out+="</li>";else out+="\n";
				}
				if(this.dataBox.genre=='DIV')out+='</ul>';
				break;
			default:
					if(this.dataBox.genre=='DIV')out+='<li>'; else out+=' * ';
					out+=this.inspect(o.varPtr);
					if(this.dataBox.genre=='DIV')out+="</li>";else out+="\n";

//				out+=this.valueToString(o.varPtr);
			}
//		if(this.dataBox.genre=='DIV')out+='</span><br>';
//		this.write(out);
		return out;
	}

}//gestionLibrairie_lib.prototype


/* ************************************************************
 * gestionLibrairie
 *
***********************************************************/
function gestionLibrairie(){
	this.erreurNo=0;this.erreurTxt=new Array();//attention tableau de tableau
	this.libNb=0;
	this.libs=new Array();
	return this;
}

gestionLibrairie.prototype=
	{
	////////////////////////////////
	//Gestion des erreurs
	erreurTranslate:function(){
		this.erreurTxt['fr']=new Array();
		this.erreurTxt['fr'][0]='ok';
	}
	,erreurShow:function(errNu,lang){
		var out='';
		for(libNu in this.libs)
			{
			if(typeof(this.libs[libNu].erreurShow)==='function')
				{
				out+=this.libs[libNu].nom+':'+this.libs[libNu].erreurShow(errNu,lang)+' ';
				}
			}
		return out;
	}

	// - creation d'une instance- //
	,instanceAdd:function(libNom,instPtr){
		var lib=this.libs[libNom];
		if(lib)lib.instanceAdd(instPtr);
	}
		
	////////////////////////////////
	,destruct:function(lib)
		{
		document.write('destruct:function(lib)<br>');
		// - lib preciser - //
		if(libNom){// -- poui: on la detruit -- //
			var lib=this.libs[libNom];
			if(lib){
				//document.write('===>this.libs[lib]:'+this.libs[lib]+'<br>');
				lib.destruct();
				lib=undefined;
				this.libNb--;
				}
		}
		else{ // -- non: on les detruits toutes -- //
			var libNu=0;
			for(libr in this.libs)
				{
//				document.write('<b>===->this.libs[libr].nom:'+this.libs[libr].nom+'</b><br>');
//				document.write('===>this.libs[libr]:'+this.libs[libr]+'<br>');
//				document.write('=== >this.libs[libr].typeof:'+this.libs[libr].typeof+'<br>');
//				this.libs[libr].destruct();
				this.libs[libr]=undefined;
				libNu++;
				if (libNu>=this.libNb){break;}
				}
			this.libs=undefined;
			this.libNb=0;
		}
	},

	////////////////////////////////
	//gestion des consoles
	end:function(libNom)
		{var lib=this.libs[libNom];if (lib)lib.end();}

	,setConsole:function(libNom,genre)
		{var lib=this.libs[libNom];if (lib)lib.setConsole(genre);}

	,clear: function(libNom)
		{var lib=this.libs[libNom];if (lib && lib.isConsole) lib.clear();}

	,evaluer:function(o)
		{var lib=this.libs[o.lib];if (lib && lib.isConsole) lib.evaluer();}

	,write: function(o)
		{var lib=this.libs[o.lib];if (lib && lib.isConsole) return lib.write(o);}

	,inspect:function(o)
		{var lib=this.libs[o.lib];if(lib && lib.isConsole)return lib.inspect(o);}

	,inspectAll:function(o)
		{var lib=this.libs[o.lib];if(lib && lib.isConsole)return lib.inspectAll(o);}

	,loadLib:function(options)
		{
		if (!options){return -1;}
		if (!options.nom){return -2;}
		if(this.libs[options.nom]==undefined ||options.force)
			{
			this.libs[options.nom]=new gestionLibrairie_lib(options);
			if (this.libs[options.nom].erreurNo===0){return this.libs[options.nom];}
			this.libNb++;
			return 0;		
			}
		return -3;
		}
	,display:function(libNom,etat)	{var lib=this.libs[libNom];if(lib.isConsole)lib.display(etat);}
	,switchShow:function(libNom)	{var lib=this.libs[libNom];if(lib.isConsole)lib.switchShow();}
	,show:function(libNom)		{var lib=this.libs[libNom];if(lib.isConsole)lib.show();}
	,hide:function(libNom)		{var lib=this.libs[libNom];if(lib.isConsole)lib.hide();}

	////////////////////////////////
	,tableau:function()
		{
		var out='';
		out+='<table class="gestLib"><caption>Librairies JavaScript ('+this.libNb+')</caption>';
		out+='<thead><tr><th>type</th><th>nom</th><th>version</th><th>err</th><th>dur&eacute;e</th>';
		out+='<th>description</th><th>url</th>';
		out+='<th title="console demander?">console</th><th title="console visible?">visible</th><th>idHTML</th><th>instances</th></tr></thead>';

		for(value in this.libs)
			{if (this.libs[value].nom)
				{
				url='';
				if (this.libs[value].url!=null){url='<a target="git" href="'+this.libs[value].url+'">lien</a>';}
				out+='<tr><td>'+this.libs[value].libType+'</td><td>'+this.libs[value].nom+'</td><td>'+this.libs[value].ver+'</td>';
				out+='<td>'+this.libs[value].erreurNo+':'+this.libs[value].erreurShow()+'</td>';
				out+='<td>'+this.libs[value].dur+'</td>';
				out+='<td>'+this.libs[value].description+'</td><td>'+url+'</td>';
				out+='<td>'+this.libs[value].isConsole+'</td><td>'+this.libs[value].isVisible+'</td><td>'+this.libs[value].idHTML+'</td>';
				out+='<td>';
	
				//erreurs liee  aux instances
				var instout='';
				for(instNu in this.libs[value].instances)
					{instout+='<b>'+instNu+':</b>';
					if(typeof(this.libs[value].instances[instNu].erreurShow)==='function')
						{instout+=this.libs[value].instances[instNu].erreurShow();}
					else	{instout+='pas de function erreurShow()';}
					instout+='<br>';
					}
				if(instout!='')out+=instout;
				out+='</td>';
				out+='</tr>\n';
				}
			};
		out+='</table>';
		return out;
		}
	} //class gestionLibrairie prototype

/* ************************************************************
instantiation du gestionnaire
 ***********************************************************/
gestLib=new gestionLibrairie();
gestLib.loadLib({nom:'gestLib'/*,idHTML:'gestLib'*/,ver:GESTLIBVERSION,description:'gestionnaire de librairies js',isConsole:0,isVisible:0,url:'http://gitorious.org/gestLib'});
gestLib.instanceAdd('gestLib',gestLib);

gestLib.end('gestLib');

/*! - ./lib/tiers/js/jscolor/jscolor-1.4.3/jscolor.js - */
gestLib.loadLib({"nom":"jscolor","ver":"1.4.3","type":"tiers","description":"JavaScript Color Picker","isConsole":1,"isVisible":1,"url":"http://jscolor.com"});

/**
 * jscolor, JavaScript Color Picker
 *
 * @version 1.4.3
 * @license GNU Lesser General Public License, http://www.gnu.org/copyleft/lesser.html
 * @author  Jan Odvarko, http://odvarko.cz
 * @created 2008-06-15
 * @updated 2014-07-16
 * @link    http://jscolor.com
 */


var jscolor = {


	dir : '', // location of jscolor directory (leave empty to autodetect)
	bindClass : 'color', // class name
	binding : true, // automatic binding via <input class="...">
	preloading : true, // use image preloading?


	install : function() {
		jscolor.addEvent(window, 'load', jscolor.init);
	},


	init : function() {
		if(jscolor.binding) {
			jscolor.bind();
		}
		if(jscolor.preloading) {
			jscolor.preload();
		}
	},


	getDir : function() {
		if(!jscolor.dir) {
			var detected = jscolor.detectDir();
			jscolor.dir = detected!==false ? detected : 'jscolor/';
		}
		return jscolor.dir;
	},


	detectDir : function() {
		var base = location.href;

		var e = document.getElementsByTagName('base');
		for(var i=0; i<e.length; i+=1) {
			if(e[i].href) { base = e[i].href; }
		}

		var e = document.getElementsByTagName('script');
		for(var i=0; i<e.length; i+=1) {
			if(e[i].src && /(^|\/)jscolor\.js([?#].*)?$/i.test(e[i].src)) {
				var src = new jscolor.URI(e[i].src);
				var srcAbs = src.toAbsolute(base);
				srcAbs.path = srcAbs.path.replace(/[^\/]+$/, ''); // remove filename
				srcAbs.query = null;
				srcAbs.fragment = null;
				return srcAbs.toString();
			}
		}
		return false;
	},


	bind : function() {
		var matchClass = new RegExp('(^|\\s)('+jscolor.bindClass+')(\\s*(\\{[^}]*\\})|\\s|$)', 'i');
		var e = document.getElementsByTagName('input');
		for(var i=0; i<e.length; i+=1) {
			var m;
			if(!e[i].color && e[i].className && (m = e[i].className.match(matchClass))) {
				var prop = {};
				if(m[4]) {
					try {
						prop = (new Function ('return (' + m[4] + ')'))();
					} catch(eInvalidProp) {}
				}
				e[i].color = new jscolor.color(e[i], prop);
			}
		}
	},


	preload : function() {
		for(var fn in jscolor.imgRequire) {
			if(jscolor.imgRequire.hasOwnProperty(fn)) {
				jscolor.loadImage(fn);
			}
		}
	},


	images : {
		pad : [ 181, 101 ],
		sld : [ 16, 101 ],
		cross : [ 15, 15 ],
		arrow : [ 7, 11 ]
	},


	imgRequire : {},
	imgLoaded : {},


	requireImage : function(filename) {
		jscolor.imgRequire[filename] = true;
	},


	loadImage : function(filename) {
		if(!jscolor.imgLoaded[filename]) {
			jscolor.imgLoaded[filename] = new Image();
			jscolor.imgLoaded[filename].src = jscolor.getDir()+filename;
		}
	},


	fetchElement : function(mixed) {
		return typeof mixed === 'string' ? document.getElementById(mixed) : mixed;
	},


	addEvent : function(el, evnt, func) {
		if(el.addEventListener) {
			el.addEventListener(evnt, func, false);
		} else if(el.attachEvent) {
			el.attachEvent('on'+evnt, func);
		}
	},


	fireEvent : function(el, evnt) {
		if(!el) {
			return;
		}
		if(document.createEvent) {
			var ev = document.createEvent('HTMLEvents');
			ev.initEvent(evnt, true, true);
			el.dispatchEvent(ev);
		} else if(document.createEventObject) {
			var ev = document.createEventObject();
			el.fireEvent('on'+evnt, ev);
		} else if(el['on'+evnt]) { // alternatively use the traditional event model (IE5)
			el['on'+evnt]();
		}
	},


	getElementPos : function(e) {
		var e1=e, e2=e;
		var x=0, y=0;
		if(e1.offsetParent) {
			do {
				x += e1.offsetLeft;
				y += e1.offsetTop;
			} while(e1 = e1.offsetParent);
		}
		while((e2 = e2.parentNode) && e2.nodeName.toUpperCase() !== 'BODY') {
			x -= e2.scrollLeft;
			y -= e2.scrollTop;
		}
		return [x, y];
	},


	getElementSize : function(e) {
		return [e.offsetWidth, e.offsetHeight];
	},


	getRelMousePos : function(e) {
		var x = 0, y = 0;
		if (!e) { e = window.event; }
		if (typeof e.offsetX === 'number') {
			x = e.offsetX;
			y = e.offsetY;
		} else if (typeof e.layerX === 'number') {
			x = e.layerX;
			y = e.layerY;
		}
		return { x: x, y: y };
	},


	getViewPos : function() {
		if(typeof window.pageYOffset === 'number') {
			return [window.pageXOffset, window.pageYOffset];
		} else if(document.body && (document.body.scrollLeft || document.body.scrollTop)) {
			return [document.body.scrollLeft, document.body.scrollTop];
		} else if(document.documentElement && (document.documentElement.scrollLeft || document.documentElement.scrollTop)) {
			return [document.documentElement.scrollLeft, document.documentElement.scrollTop];
		} else {
			return [0, 0];
		}
	},


	getViewSize : function() {
		if(typeof window.innerWidth === 'number') {
			return [window.innerWidth, window.innerHeight];
		} else if(document.body && (document.body.clientWidth || document.body.clientHeight)) {
			return [document.body.clientWidth, document.body.clientHeight];
		} else if(document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
			return [document.documentElement.clientWidth, document.documentElement.clientHeight];
		} else {
			return [0, 0];
		}
	},


	URI : function(uri) { // See RFC3986

		this.scheme = null;
		this.authority = null;
		this.path = '';
		this.query = null;
		this.fragment = null;

		this.parse = function(uri) {
			var m = uri.match(/^(([A-Za-z][0-9A-Za-z+.-]*)(:))?((\/\/)([^\/?#]*))?([^?#]*)((\?)([^#]*))?((#)(.*))?/);
			this.scheme = m[3] ? m[2] : null;
			this.authority = m[5] ? m[6] : null;
			this.path = m[7];
			this.query = m[9] ? m[10] : null;
			this.fragment = m[12] ? m[13] : null;
			return this;
		};

		this.toString = function() {
			var result = '';
			if(this.scheme !== null) { result = result + this.scheme + ':'; }
			if(this.authority !== null) { result = result + '//' + this.authority; }
			if(this.path !== null) { result = result + this.path; }
			if(this.query !== null) { result = result + '?' + this.query; }
			if(this.fragment !== null) { result = result + '#' + this.fragment; }
			return result;
		};

		this.toAbsolute = function(base) {
			var base = new jscolor.URI(base);
			var r = this;
			var t = new jscolor.URI;

			if(base.scheme === null) { return false; }

			if(r.scheme !== null && r.scheme.toLowerCase() === base.scheme.toLowerCase()) {
				r.scheme = null;
			}

			if(r.scheme !== null) {
				t.scheme = r.scheme;
				t.authority = r.authority;
				t.path = removeDotSegments(r.path);
				t.query = r.query;
			} else {
				if(r.authority !== null) {
					t.authority = r.authority;
					t.path = removeDotSegments(r.path);
					t.query = r.query;
				} else {
					if(r.path === '') {
						t.path = base.path;
						if(r.query !== null) {
							t.query = r.query;
						} else {
							t.query = base.query;
						}
					} else {
						if(r.path.substr(0,1) === '/') {
							t.path = removeDotSegments(r.path);
						} else {
							if(base.authority !== null && base.path === '') {
								t.path = '/'+r.path;
							} else {
								t.path = base.path.replace(/[^\/]+$/,'')+r.path;
							}
							t.path = removeDotSegments(t.path);
						}
						t.query = r.query;
					}
					t.authority = base.authority;
				}
				t.scheme = base.scheme;
			}
			t.fragment = r.fragment;

			return t;
		};

		function removeDotSegments(path) {
			var out = '';
			while(path) {
				if(path.substr(0,3)==='../' || path.substr(0,2)==='./') {
					path = path.replace(/^\.+/,'').substr(1);
				} else if(path.substr(0,3)==='/./' || path==='/.') {
					path = '/'+path.substr(3);
				} else if(path.substr(0,4)==='/../' || path==='/..') {
					path = '/'+path.substr(4);
					out = out.replace(/\/?[^\/]*$/, '');
				} else if(path==='.' || path==='..') {
					path = '';
				} else {
					var rm = path.match(/^\/?[^\/]*/)[0];
					path = path.substr(rm.length);
					out = out + rm;
				}
			}
			return out;
		}

		if(uri) {
			this.parse(uri);
		}

	},


	//
	// Usage example:
	// var myColor = new jscolor.color(myInputElement)
	//

	color : function(target, prop) {


		this.required = true; // refuse empty values?
		this.adjust = true; // adjust value to uniform notation?
		this.hash = false; // prefix color with # symbol?
		this.caps = true; // uppercase?
		this.slider = true; // show the value/saturation slider?
		this.valueElement = target; // value holder
		this.styleElement = target; // where to reflect current color
		this.onImmediateChange = null; // onchange callback (can be either string or function)
		this.hsv = [0, 0, 1]; // read-only  0-6, 0-1, 0-1
		this.rgb = [1, 1, 1]; // read-only  0-1, 0-1, 0-1
		this.minH = 0; // read-only  0-6
		this.maxH = 6; // read-only  0-6
		this.minS = 0; // read-only  0-1
		this.maxS = 1; // read-only  0-1
		this.minV = 0; // read-only  0-1
		this.maxV = 1; // read-only  0-1

		this.pickerOnfocus = true; // display picker on focus?
		this.pickerMode = 'HSV'; // HSV | HVS
		this.pickerPosition = 'bottom'; // left | right | top | bottom
		this.pickerSmartPosition = true; // automatically adjust picker position when necessary
		this.pickerButtonHeight = 20; // px
		this.pickerClosable = false;
		this.pickerCloseText = 'Close';
		this.pickerButtonColor = 'ButtonText'; // px
		this.pickerFace = 10; // px
		this.pickerFaceColor = 'ThreeDFace'; // CSS color
		this.pickerBorder = 1; // px
		this.pickerBorderColor = 'ThreeDHighlight ThreeDShadow ThreeDShadow ThreeDHighlight'; // CSS color
		this.pickerInset = 1; // px
		this.pickerInsetColor = 'ThreeDShadow ThreeDHighlight ThreeDHighlight ThreeDShadow'; // CSS color
		this.pickerZIndex = 10000;


		for(var p in prop) {
			if(prop.hasOwnProperty(p)) {
				this[p] = prop[p];
			}
		}


		this.hidePicker = function() {
			if(isPickerOwner()) {
				removePicker();
			}
		};


		this.showPicker = function() {
			if(!isPickerOwner()) {
				var tp = jscolor.getElementPos(target); // target pos
				var ts = jscolor.getElementSize(target); // target size
				var vp = jscolor.getViewPos(); // view pos
				var vs = jscolor.getViewSize(); // view size
				var ps = getPickerDims(this); // picker size
				var a, b, c;
				switch(this.pickerPosition.toLowerCase()) {
					case 'left': a=1; b=0; c=-1; break;
					case 'right':a=1; b=0; c=1; break;
					case 'top':  a=0; b=1; c=-1; break;
					default:     a=0; b=1; c=1; break;
				}
				var l = (ts[b]+ps[b])/2;

				// picker pos
				if (!this.pickerSmartPosition) {
					var pp = [
						tp[a],
						tp[b]+ts[b]-l+l*c
					];
				} else {
					var pp = [
						-vp[a]+tp[a]+ps[a] > vs[a] ?
							(-vp[a]+tp[a]+ts[a]/2 > vs[a]/2 && tp[a]+ts[a]-ps[a] >= 0 ? tp[a]+ts[a]-ps[a] : tp[a]) :
							tp[a],
						-vp[b]+tp[b]+ts[b]+ps[b]-l+l*c > vs[b] ?
							(-vp[b]+tp[b]+ts[b]/2 > vs[b]/2 && tp[b]+ts[b]-l-l*c >= 0 ? tp[b]+ts[b]-l-l*c : tp[b]+ts[b]-l+l*c) :
							(tp[b]+ts[b]-l+l*c >= 0 ? tp[b]+ts[b]-l+l*c : tp[b]+ts[b]-l-l*c)
					];
				}
				drawPicker(pp[a], pp[b]);
			}
		};


		this.importColor = function() {
			if(!valueElement) {
				this.exportColor();
			} else {
				if(!this.adjust) {
					if(!this.fromString(valueElement.value, leaveValue)) {
						styleElement.style.backgroundImage = styleElement.jscStyle.backgroundImage;
						styleElement.style.backgroundColor = styleElement.jscStyle.backgroundColor;
						styleElement.style.color = styleElement.jscStyle.color;
						this.exportColor(leaveValue | leaveStyle);
					}
				} else if(!this.required && /^\s*$/.test(valueElement.value)) {
					valueElement.value = '';
					styleElement.style.backgroundImage = styleElement.jscStyle.backgroundImage;
					styleElement.style.backgroundColor = styleElement.jscStyle.backgroundColor;
					styleElement.style.color = styleElement.jscStyle.color;
					this.exportColor(leaveValue | leaveStyle);

				} else if(this.fromString(valueElement.value)) {
					// OK
				} else {
					this.exportColor();
				}
			}
		};


		this.exportColor = function(flags) {
			if(!(flags & leaveValue) && valueElement) {
				var value = this.toString();
				if(this.caps) { value = value.toUpperCase(); }
				if(this.hash) { value = '#'+value; }
				valueElement.value = value;
			}
			if(!(flags & leaveStyle) && styleElement) {
				styleElement.style.backgroundImage = "none";
				styleElement.style.backgroundColor =
					'#'+this.toString();
				styleElement.style.color =
					0.213 * this.rgb[0] +
					0.715 * this.rgb[1] +
					0.072 * this.rgb[2]
					< 0.5 ? '#FFF' : '#000';
			}
			if(!(flags & leavePad) && isPickerOwner()) {
				redrawPad();
			}
			if(!(flags & leaveSld) && isPickerOwner()) {
				redrawSld();
			}
		};


		this.fromHSV = function(h, s, v, flags) { // null = don't change
			if(h !== null) { h = Math.max(0.0, this.minH, Math.min(6.0, this.maxH, h)); }
			if(s !== null) { s = Math.max(0.0, this.minS, Math.min(1.0, this.maxS, s)); }
			if(v !== null) { v = Math.max(0.0, this.minV, Math.min(1.0, this.maxV, v)); }

			this.rgb = HSV_RGB(
				h===null ? this.hsv[0] : (this.hsv[0]=h),
				s===null ? this.hsv[1] : (this.hsv[1]=s),
				v===null ? this.hsv[2] : (this.hsv[2]=v)
			);

			this.exportColor(flags);
		};


		this.fromRGB = function(r, g, b, flags) { // null = don't change
			if(r !== null) { r = Math.max(0.0, Math.min(1.0, r)); }
			if(g !== null) { g = Math.max(0.0, Math.min(1.0, g)); }
			if(b !== null) { b = Math.max(0.0, Math.min(1.0, b)); }

			var hsv = RGB_HSV(
				r===null ? this.rgb[0] : r,
				g===null ? this.rgb[1] : g,
				b===null ? this.rgb[2] : b
			);
			if(hsv[0] !== null) {
				this.hsv[0] = Math.max(0.0, this.minH, Math.min(6.0, this.maxH, hsv[0]));
			}
			if(hsv[2] !== 0) {
				this.hsv[1] = hsv[1]===null ? null : Math.max(0.0, this.minS, Math.min(1.0, this.maxS, hsv[1]));
			}
			this.hsv[2] = hsv[2]===null ? null : Math.max(0.0, this.minV, Math.min(1.0, this.maxV, hsv[2]));

			// update RGB according to final HSV, as some values might be trimmed
			var rgb = HSV_RGB(this.hsv[0], this.hsv[1], this.hsv[2]);
			this.rgb[0] = rgb[0];
			this.rgb[1] = rgb[1];
			this.rgb[2] = rgb[2];

			this.exportColor(flags);
		};


		this.fromString = function(hex, flags) {
			var m = hex.match(/^\W*([0-9A-F]{3}([0-9A-F]{3})?)\W*$/i);
			if(!m) {
				return false;
			} else {
				if(m[1].length === 6) { // 6-char notation
					this.fromRGB(
						parseInt(m[1].substr(0,2),16) / 255,
						parseInt(m[1].substr(2,2),16) / 255,
						parseInt(m[1].substr(4,2),16) / 255,
						flags
					);
				} else { // 3-char notation
					this.fromRGB(
						parseInt(m[1].charAt(0)+m[1].charAt(0),16) / 255,
						parseInt(m[1].charAt(1)+m[1].charAt(1),16) / 255,
						parseInt(m[1].charAt(2)+m[1].charAt(2),16) / 255,
						flags
					);
				}
				return true;
			}
		};


		this.toString = function() {
			return (
				(0x100 | Math.round(255*this.rgb[0])).toString(16).substr(1) +
				(0x100 | Math.round(255*this.rgb[1])).toString(16).substr(1) +
				(0x100 | Math.round(255*this.rgb[2])).toString(16).substr(1)
			);
		};


		function RGB_HSV(r, g, b) {
			var n = Math.min(Math.min(r,g),b);
			var v = Math.max(Math.max(r,g),b);
			var m = v - n;
			if(m === 0) { return [ null, 0, v ]; }
			var h = r===n ? 3+(b-g)/m : (g===n ? 5+(r-b)/m : 1+(g-r)/m);
			return [ h===6?0:h, m/v, v ];
		}


		function HSV_RGB(h, s, v) {
			if(h === null) { return [ v, v, v ]; }
			var i = Math.floor(h);
			var f = i%2 ? h-i : 1-(h-i);
			var m = v * (1 - s);
			var n = v * (1 - s*f);
			switch(i) {
				case 6:
				case 0: return [v,n,m];
				case 1: return [n,v,m];
				case 2: return [m,v,n];
				case 3: return [m,n,v];
				case 4: return [n,m,v];
				case 5: return [v,m,n];
			}
		}


		function removePicker() {
			delete jscolor.picker.owner;
			document.getElementsByTagName('body')[0].removeChild(jscolor.picker.boxB);
		}


		function drawPicker(x, y) {
			if(!jscolor.picker) {
				jscolor.picker = {
					box : document.createElement('div'),
					boxB : document.createElement('div'),
					pad : document.createElement('div'),
					padB : document.createElement('div'),
					padM : document.createElement('div'),
					sld : document.createElement('div'),
					sldB : document.createElement('div'),
					sldM : document.createElement('div'),
					btn : document.createElement('div'),
					btnS : document.createElement('span'),
					btnT : document.createTextNode(THIS.pickerCloseText)
				};
				for(var i=0,segSize=4; i<jscolor.images.sld[1]; i+=segSize) {
					var seg = document.createElement('div');
					seg.style.height = segSize+'px';
					seg.style.fontSize = '1px';
					seg.style.lineHeight = '0';
					jscolor.picker.sld.appendChild(seg);
				}
				jscolor.picker.sldB.appendChild(jscolor.picker.sld);
				jscolor.picker.box.appendChild(jscolor.picker.sldB);
				jscolor.picker.box.appendChild(jscolor.picker.sldM);
				jscolor.picker.padB.appendChild(jscolor.picker.pad);
				jscolor.picker.box.appendChild(jscolor.picker.padB);
				jscolor.picker.box.appendChild(jscolor.picker.padM);
				jscolor.picker.btnS.appendChild(jscolor.picker.btnT);
				jscolor.picker.btn.appendChild(jscolor.picker.btnS);
				jscolor.picker.box.appendChild(jscolor.picker.btn);
				jscolor.picker.boxB.appendChild(jscolor.picker.box);
			}

			var p = jscolor.picker;

			// controls interaction
			p.box.onmouseup =
			p.box.onmouseout = function() { target.focus(); };
			p.box.onmousedown = function() { abortBlur=true; };
			p.box.onmousemove = function(e) {
				if (holdPad || holdSld) {
					holdPad && setPad(e);
					holdSld && setSld(e);
					if (document.selection) {
						document.selection.empty();
					} else if (window.getSelection) {
						window.getSelection().removeAllRanges();
					}
					dispatchImmediateChange();
				}
			};
			if('ontouchstart' in window) { // if touch device
				var handle_touchmove = function(e) {
					var event={
						'offsetX': e.touches[0].pageX-touchOffset.X,
						'offsetY': e.touches[0].pageY-touchOffset.Y
					};
					if (holdPad || holdSld) {
						holdPad && setPad(event);
						holdSld && setSld(event);
						dispatchImmediateChange();
					}
					e.stopPropagation(); // prevent move "view" on broswer
					e.preventDefault(); // prevent Default - Android Fix (else android generated only 1-2 touchmove events)
				};
				p.box.removeEventListener('touchmove', handle_touchmove, false)
				p.box.addEventListener('touchmove', handle_touchmove, false)
			}
			p.padM.onmouseup =
			p.padM.onmouseout = function() { if(holdPad) { holdPad=false; jscolor.fireEvent(valueElement,'change'); } };
			p.padM.onmousedown = function(e) {
				// if the slider is at the bottom, move it up
				switch(modeID) {
					case 0: if (THIS.hsv[2] === 0) { THIS.fromHSV(null, null, 1.0); }; break;
					case 1: if (THIS.hsv[1] === 0) { THIS.fromHSV(null, 1.0, null); }; break;
				}
				holdSld=false;
				holdPad=true;
				setPad(e);
				dispatchImmediateChange();
			};
			if('ontouchstart' in window) {
				p.padM.addEventListener('touchstart', function(e) {
					touchOffset={
						'X': e.target.offsetParent.offsetLeft,
						'Y': e.target.offsetParent.offsetTop
					};
					this.onmousedown({
						'offsetX':e.touches[0].pageX-touchOffset.X,
						'offsetY':e.touches[0].pageY-touchOffset.Y
					});
				});
			}
			p.sldM.onmouseup =
			p.sldM.onmouseout = function() { if(holdSld) { holdSld=false; jscolor.fireEvent(valueElement,'change'); } };
			p.sldM.onmousedown = function(e) {
				holdPad=false;
				holdSld=true;
				setSld(e);
				dispatchImmediateChange();
			};
			if('ontouchstart' in window) {
				p.sldM.addEventListener('touchstart', function(e) {
					touchOffset={
						'X': e.target.offsetParent.offsetLeft,
						'Y': e.target.offsetParent.offsetTop
					};
					this.onmousedown({
						'offsetX':e.touches[0].pageX-touchOffset.X,
						'offsetY':e.touches[0].pageY-touchOffset.Y
					});
				});
			}

			// picker
			var dims = getPickerDims(THIS);
			p.box.style.width = dims[0] + 'px';
			p.box.style.height = dims[1] + 'px';

			// picker border
			p.boxB.style.position = 'absolute';
			p.boxB.style.clear = 'both';
			p.boxB.style.left = x+'px';
			p.boxB.style.top = y+'px';
			p.boxB.style.zIndex = THIS.pickerZIndex;
			p.boxB.style.border = THIS.pickerBorder+'px solid';
			p.boxB.style.borderColor = THIS.pickerBorderColor;
			p.boxB.style.background = THIS.pickerFaceColor;

			// pad image
			p.pad.style.width = jscolor.images.pad[0]+'px';
			p.pad.style.height = jscolor.images.pad[1]+'px';

			// pad border
			p.padB.style.position = 'absolute';
			p.padB.style.left = THIS.pickerFace+'px';
			p.padB.style.top = THIS.pickerFace+'px';
			p.padB.style.border = THIS.pickerInset+'px solid';
			p.padB.style.borderColor = THIS.pickerInsetColor;

			// pad mouse area
			p.padM.style.position = 'absolute';
			p.padM.style.left = '0';
			p.padM.style.top = '0';
			p.padM.style.width = THIS.pickerFace + 2*THIS.pickerInset + jscolor.images.pad[0] + jscolor.images.arrow[0] + 'px';
			p.padM.style.height = p.box.style.height;
			p.padM.style.cursor = 'crosshair';

			// slider image
			p.sld.style.overflow = 'hidden';
			p.sld.style.width = jscolor.images.sld[0]+'px';
			p.sld.style.height = jscolor.images.sld[1]+'px';

			// slider border
			p.sldB.style.display = THIS.slider ? 'block' : 'none';
			p.sldB.style.position = 'absolute';
			p.sldB.style.right = THIS.pickerFace+'px';
			p.sldB.style.top = THIS.pickerFace+'px';
			p.sldB.style.border = THIS.pickerInset+'px solid';
			p.sldB.style.borderColor = THIS.pickerInsetColor;

			// slider mouse area
			p.sldM.style.display = THIS.slider ? 'block' : 'none';
			p.sldM.style.position = 'absolute';
			p.sldM.style.right = '0';
			p.sldM.style.top = '0';
			p.sldM.style.width = jscolor.images.sld[0] + jscolor.images.arrow[0] + THIS.pickerFace + 2*THIS.pickerInset + 'px';
			p.sldM.style.height = p.box.style.height;
			try {
				p.sldM.style.cursor = 'pointer';
			} catch(eOldIE) {
				p.sldM.style.cursor = 'hand';
			}

			// "close" button
			function setBtnBorder() {
				var insetColors = THIS.pickerInsetColor.split(/\s+/);
				var pickerOutsetColor = insetColors.length < 2 ? insetColors[0] : insetColors[1] + ' ' + insetColors[0] + ' ' + insetColors[0] + ' ' + insetColors[1];
				p.btn.style.borderColor = pickerOutsetColor;
			}
			p.btn.style.display = THIS.pickerClosable ? 'block' : 'none';
			p.btn.style.position = 'absolute';
			p.btn.style.left = THIS.pickerFace + 'px';
			p.btn.style.bottom = THIS.pickerFace + 'px';
			p.btn.style.padding = '0 15px';
			p.btn.style.height = '18px';
			p.btn.style.border = THIS.pickerInset + 'px solid';
			setBtnBorder();
			p.btn.style.color = THIS.pickerButtonColor;
			p.btn.style.font = '12px sans-serif';
			p.btn.style.textAlign = 'center';
			try {
				p.btn.style.cursor = 'pointer';
			} catch(eOldIE) {
				p.btn.style.cursor = 'hand';
			}
			p.btn.onmousedown = function () {
				THIS.hidePicker();
			};
			p.btnS.style.lineHeight = p.btn.style.height;

			// load images in optimal order
			switch(modeID) {
				case 0: var padImg = 'hs.png'; break;
				case 1: var padImg = 'hv.png'; break;
			}
			p.padM.style.backgroundImage = "url('"+jscolor.getDir()+"cross.gif')";
			p.padM.style.backgroundRepeat = "no-repeat";
			p.sldM.style.backgroundImage = "url('"+jscolor.getDir()+"arrow.gif')";
			p.sldM.style.backgroundRepeat = "no-repeat";
			p.pad.style.backgroundImage = "url('"+jscolor.getDir()+padImg+"')";
			p.pad.style.backgroundRepeat = "no-repeat";
			p.pad.style.backgroundPosition = "0 0";

			// place pointers
			redrawPad();
			redrawSld();

			jscolor.picker.owner = THIS;
			document.getElementsByTagName('body')[0].appendChild(p.boxB);
		}


		function getPickerDims(o) {
			var dims = [
				2*o.pickerInset + 2*o.pickerFace + jscolor.images.pad[0] +
					(o.slider ? 2*o.pickerInset + 2*jscolor.images.arrow[0] + jscolor.images.sld[0] : 0),
				o.pickerClosable ?
					4*o.pickerInset + 3*o.pickerFace + jscolor.images.pad[1] + o.pickerButtonHeight :
					2*o.pickerInset + 2*o.pickerFace + jscolor.images.pad[1]
			];
			return dims;
		}


		function redrawPad() {
			// redraw the pad pointer
			switch(modeID) {
				case 0: var yComponent = 1; break;
				case 1: var yComponent = 2; break;
			}
			var x = Math.round((THIS.hsv[0]/6) * (jscolor.images.pad[0]-1));
			var y = Math.round((1-THIS.hsv[yComponent]) * (jscolor.images.pad[1]-1));
			jscolor.picker.padM.style.backgroundPosition =
				(THIS.pickerFace+THIS.pickerInset+x - Math.floor(jscolor.images.cross[0]/2)) + 'px ' +
				(THIS.pickerFace+THIS.pickerInset+y - Math.floor(jscolor.images.cross[1]/2)) + 'px';

			// redraw the slider image
			var seg = jscolor.picker.sld.childNodes;

			switch(modeID) {
				case 0:
					var rgb = HSV_RGB(THIS.hsv[0], THIS.hsv[1], 1);
					for(var i=0; i<seg.length; i+=1) {
						seg[i].style.backgroundColor = 'rgb('+
							(rgb[0]*(1-i/seg.length)*100)+'%,'+
							(rgb[1]*(1-i/seg.length)*100)+'%,'+
							(rgb[2]*(1-i/seg.length)*100)+'%)';
					}
					break;
				case 1:
					var rgb, s, c = [ THIS.hsv[2], 0, 0 ];
					var i = Math.floor(THIS.hsv[0]);
					var f = i%2 ? THIS.hsv[0]-i : 1-(THIS.hsv[0]-i);
					switch(i) {
						case 6:
						case 0: rgb=[0,1,2]; break;
						case 1: rgb=[1,0,2]; break;
						case 2: rgb=[2,0,1]; break;
						case 3: rgb=[2,1,0]; break;
						case 4: rgb=[1,2,0]; break;
						case 5: rgb=[0,2,1]; break;
					}
					for(var i=0; i<seg.length; i+=1) {
						s = 1 - 1/(seg.length-1)*i;
						c[1] = c[0] * (1 - s*f);
						c[2] = c[0] * (1 - s);
						seg[i].style.backgroundColor = 'rgb('+
							(c[rgb[0]]*100)+'%,'+
							(c[rgb[1]]*100)+'%,'+
							(c[rgb[2]]*100)+'%)';
					}
					break;
			}
		}


		function redrawSld() {
			// redraw the slider pointer
			switch(modeID) {
				case 0: var yComponent = 2; break;
				case 1: var yComponent = 1; break;
			}
			var y = Math.round((1-THIS.hsv[yComponent]) * (jscolor.images.sld[1]-1));
			jscolor.picker.sldM.style.backgroundPosition =
				'0 ' + (THIS.pickerFace+THIS.pickerInset+y - Math.floor(jscolor.images.arrow[1]/2)) + 'px';
		}


		function isPickerOwner() {
			return jscolor.picker && jscolor.picker.owner === THIS;
		}


		function blurTarget() {
			if(valueElement === target) {
				THIS.importColor();
			}
			if(THIS.pickerOnfocus) {
				THIS.hidePicker();
			}
		}


		function blurValue() {
			if(valueElement !== target) {
				THIS.importColor();
			}
		}


		function setPad(e) {
			var mpos = jscolor.getRelMousePos(e);
			var x = mpos.x - THIS.pickerFace - THIS.pickerInset;
			var y = mpos.y - THIS.pickerFace - THIS.pickerInset;
			switch(modeID) {
				case 0: THIS.fromHSV(x*(6/(jscolor.images.pad[0]-1)), 1 - y/(jscolor.images.pad[1]-1), null, leaveSld); break;
				case 1: THIS.fromHSV(x*(6/(jscolor.images.pad[0]-1)), null, 1 - y/(jscolor.images.pad[1]-1), leaveSld); break;
			}
		}


		function setSld(e) {
			var mpos = jscolor.getRelMousePos(e);
			var y = mpos.y - THIS.pickerFace - THIS.pickerInset;
			switch(modeID) {
				case 0: THIS.fromHSV(null, null, 1 - y/(jscolor.images.sld[1]-1), leavePad); break;
				case 1: THIS.fromHSV(null, 1 - y/(jscolor.images.sld[1]-1), null, leavePad); break;
			}
		}


		function dispatchImmediateChange() {
			if (THIS.onImmediateChange) {
				var callback;
				if (typeof THIS.onImmediateChange === 'string') {
					callback = new Function (THIS.onImmediateChange);
				} else {
					callback = THIS.onImmediateChange;
				}
				callback.call(THIS);
			}
		}


		var THIS = this;
		var modeID = this.pickerMode.toLowerCase()==='hvs' ? 1 : 0;
		var abortBlur = false;
		var
			valueElement = jscolor.fetchElement(this.valueElement),
			styleElement = jscolor.fetchElement(this.styleElement);
		var
			holdPad = false,
			holdSld = false,
			touchOffset = {};
		var
			leaveValue = 1<<0,
			leaveStyle = 1<<1,
			leavePad = 1<<2,
			leaveSld = 1<<3;

		// target
		jscolor.addEvent(target, 'focus', function() {
			if(THIS.pickerOnfocus) { THIS.showPicker(); }
		});
		jscolor.addEvent(target, 'blur', function() {
			if(!abortBlur) {
				window.setTimeout(function(){ abortBlur || blurTarget(); abortBlur=false; }, 0);
			} else {
				abortBlur = false;
			}
		});

		// valueElement
		if(valueElement) {
			var updateField = function() {
				THIS.fromString(valueElement.value, leaveValue);
				dispatchImmediateChange();
			};
			jscolor.addEvent(valueElement, 'keyup', updateField);
			jscolor.addEvent(valueElement, 'input', updateField);
			jscolor.addEvent(valueElement, 'blur', blurValue);
			valueElement.setAttribute('autocomplete', 'off');
		}

		// styleElement
		if(styleElement) {
			styleElement.jscStyle = {
				backgroundImage : styleElement.style.backgroundImage,
				backgroundColor : styleElement.style.backgroundColor,
				color : styleElement.style.color
			};
		}

		// require images
		switch(modeID) {
			case 0: jscolor.requireImage('hs.png'); break;
			case 1: jscolor.requireImage('hv.png'); break;
		}
		jscolor.requireImage('cross.gif');
		jscolor.requireImage('arrow.gif');

		this.importColor();
	}

};


jscolor.install();
gestLib.end("jscolor");


/*! - /www/git/intersites/lib/legral/js/gestClasseurs/gestClasseurs.js - */
/*!
fichier: gestClasseurs.js
auteur:pascal TOLEDO
date de creation : 2014.12.05
source: http://gitorious.org/gestClasseurs
depend de:
  * rien
description: cf README
*/

GESTCLASSEURSVERSION= '0.2.1';

if(typeof(gestLib)==='object')gestLib.loadLib({nom:'gestClasseurs',ver:GESTCLASSEURSVERSION,description:'creation de div dynamique en js',isConsole:0,isVisible:0,url:'http://gitorious.org/gestClasseurs'});


/*
function classeurs_menuVisible(options)
	{
	if(!options){return -1;}
	if(!options.classeuridJS){return -2;}
	if(!options.feuilleNom){return -3;}
	options.classeuridJS.show()
	}
*/

//options: nom:obligatoire
//erreur:
// -1: absence d'options
// -10: la requete Ajax a echouer


/*
 * function TclasseursFeuille(classeurNom,options)
 * param obligatorie:
 * nom: pas d'espace ni de caractere spéciaux (sert a calcule l'IDHTML des enfants)
 * classeurIDJS
 */
function TclasseursFeuille(f){
	this.methodRun='TclasseurFeuille';
	this.isLoad=0;
	if(typeof(f)!='object')return -1;
	this.nom=f.nom;				// pas d'espace ni de caractere spéciaux (sert a calcule l'IDHTML des enfants)

	// - - //
	this.classeurIDJS=f.classeurIDJS;
	this.description=f.description;	// description de la feuille

	// -  menu: support des onglets - //
	this.menuIDCSS=this.classeurIDJS.menuIDCSS;
	this.ongletIDCSS=null;
	this.titre=(f.titre)?f.titre:f.nom;	// titre de l'onglet (texte afficher)

	// - datasBoxSupport - //
	this.datasBoxSupportIDCSS=this.classeurIDJS.datasBoxSupportIDCSS;
	this.datasBoxIDCSS=null;	// - IDCSS de la dataBox contenant le texte
	this.texte=f.texte;			// -  contenu de la dataBox
	//	this.liidCSS=null;

	this.description=f.description;

	// - creation de la feuille: (onglet + dataBox)- //
	this.loadFeuille();
	this.isLoad=1;
	return this;
	} // class Tclasseur

TclasseursFeuille.prototype=
	{
	destruct:function()
		{
		document.getElementById(this.support+'_li').remove();
		document.getElementById(this.support+'_datas').remove();
		this.feuille=undefined;
		}
	,onClick:function(elt,f){
	}

	,loadFeuille:function()
		{
		this.methodRun='TclasseurFeuille:loadFeuille';

		// - creation de l'onglet dans le menu - //
		this.ongletIDCSS=document.createElement('span');
		this.ongletIDCSS.id=this.classeurIDJS.nom+'_'+this.nom+'_onglet';	// utile?
		this.ongletIDCSS.className="gestClasseurs_onglet";
		this.ongletIDCSS.innerHTML=this.titre;
		this.menuIDCSS.appendChild(this.ongletIDCSS);

		var _classeurIDJS=this.classeurIDJS; // pour envoyer dans la fonction anonyme
		var _feuilleIDJS=this;

		// - activation du click sur le menu - //
		this.ongletIDCSS.onclick=function(elt){
//				gestLib.inspect({"lib":"gestClasseurs","varNom":"_classeurIDJS.nom","varPtr": _classeurIDJS.nom});
//				gestLib.inspect({"lib":"gestClasseurs","varNom":"_feuilleIDJS.nom","varPtr":  _feuilleIDJS.nom});
				_classeurIDJS.select(_feuilleIDJS.nom);


				// - afficher la dataBox - //
				
				}



		// - creation de la dataBox dans la datasBoxSupport - //
		this.datasBoxIDCSS=document.createElement('div');
		this.datasBoxIDCSS.id=this.classeurIDJS.nom+'_'+this.nom+'_datasBox';	// utile?
		this.datasBoxIDCSS.className="gestClasseurs_datasBox";
		this.datasBoxIDCSS.innerHTML=this.texte;
		this.datasBoxSupportIDCSS.appendChild(this.datasBoxIDCSS);

	} // loadFeuille
	// - selectionne l'onglet- //
	,select:function(){
		// -- mise en style -- //
		this.ongletIDCSS.className+=' gestClasseurs_ongletSelected';
		this.datasBoxIDCSS.style.display='block';

	}
	,unselect:function(){
		// -- mise en style -- //
		this.ongletIDCSS.className=this.ongletIDCSS.className.replace(' gestClasseurs_ongletSelected','');
		this.datasBoxIDCSS.style.display='none';

	}

	,show:function(all){this.liidCSS.style.visibility='visible';this.datasidCSS.style.visibility='visible';}
	,hide:function(all)
		{
		if(all){this.liidCSS.style.visibility='hidden';}
		this.datasidCSS.style.visibility='hidden';
		}
	,write:function(texte){
		/*this.texte+=txt;this.datasidCSS.innerHTML=this.texte;*/
		this.datasBoxIDCSS.innerHTML=texte;
	}
	}

/*********************
 *
 *
 ********************/

function Tclasseur(c)
	{
	this.erreur=0;
	this.methodRun='Tclasseur';
	if(typeof(c)!='object'){this.erreur=-1;return -1;}
	if(!c.nom){this.erreur=-2;return -2;}
//	this.options=options;
	this.nom=c.nom;

	// - support general - //
	this.htmlID=(c.htmlID)?c.htmlID:c.nom;
	this.htmlIDCSS=document.getElementById(this.htmlID);
	if(!this.htmlIDCSS){this.erreur=-3;return -3;}


	// - support du menu des onglets - //
	//this.classeur_datas_supportidCSS=null;
	this.menuIDCSS=null;
	this.datasBoxSupportIDCSS=null;
	this.menuTitre=c.menuTitre?c.menuTitre:'';

	// - - //
	this.feuilleNb=0;
	this.feuilleDetruiteNb=0;
	this.feuilles=new Array();
	this.creerSupport();
	return this;
	}//class Tclasseur

Tclasseur.prototype =
	{
	destruct:function(feuille)
		{
		if(feuille)
			{if(this.feuilles[feuille]!=undefined&&'unload')
				{this.feuilles[feuille].destruct();this.feuilles[feuille]='unload';this.feuilleDetruiteNb++;}
				}
		else	{
			var feuilleNu=0;
			for(feuiller in this.feuilles)
				{
				this.feuilles[feuiller].destruct();
				this.feuilles[feuiller]='unload';
				feuilleNu++;
				if(feuilleNu>=this.feuilleNb){break;}
				}
			this.feuilles='unload';
			this.feuilles=new Array();
			this.feuilleDetruiteNb=0;
			this.feuilleNb=0;
			}
		},
	clean:function(nom)
		{var temp=new Array();
		for (var key in this.feuilles)
			{
			if(this.feuilles[key]!='unload'&&key!=nom){temp[key]=this.feuilles[key];}
			this.feuilleNb--;
			}
		this.feuilles=temp;
		},
		
	creerSupport:function(){

		// - creation du support des menus - //
		this.menuIDCSS=document.createElement('div');
		this.menuIDCSS.id=this.nom+'_onglets';	// utile?
		this.menuIDCSS.className="gestClasseurs_menuSupport";
		this.menuIDCSS.innerHTML=this.menuTitre;
		this.htmlIDCSS.appendChild(this.menuIDCSS); 

		// - creation du support des dataBox - //
		this.datasBoxSupportIDCSS=document.createElement('div');
		this.datasBoxSupportIDCSS.id=this.nom+'_datasBoxSupport';	// utile?
		this.datasBoxSupportIDCSS.className="gestClasseurs_datasBoxSupport";
		this.datasBoxSupportIDCSS.innerHTML="datasBoxSupport";
		this.htmlIDCSS.appendChild(this.datasBoxSupportIDCSS); 
	}

	,loadFeuille:function(f){
		this.methodRun='Tclasseur';
		this.erreur=0;
		if(typeof(f)!='object'){this.erreur=-1;return -1;}
		if(!f.nom){this.erreur=-2;return -2;}		// la feuille n'a pas de nom
		
		if(this.feuilles[f.nom]==undefined||f.forcer){
			this.destruct(f.nom);

			f.classeurIDJS=this;

			this.feuilles[f.nom]=new TclasseursFeuille(f);

			// - supprimer la feuille en cas d'erreur - //
			if(this.feuilles[f.nom].erreur<0){
				this.feuilleNb--;
				erreur=this.feuilles[f.nom].erreur;
				this.destruct(this.f.nom);
				return erreur;
			}
		}

	}// loadFeuille()

	,select:function(nom){
		var f=this.feuilles[nom];
		this.unselectAll();
		f.select();
	}
	
	,unselectAll:function(){
		for(var key in this.feuilles){
			//if(this.feuilles[key]!=undefined&&this.feuilles[key]!='unload')
			var f=this.feuilles[key];
			if(f && f.nom)
				{f.unselect();}
		}

	}
	,show:function(nom){

		if(this.feuilles[nom]!=undefined&&this.feuilles[nom]!='unload')
			{
			this.hide();
			this.feuilles[nom].show();
			}
	}

	,hide:function(nom)
		{
		// - si un nom de feuille est donner alors ne ca- //
		if(!nom)
			{
			for(var key in this.feuilles)
				{

				//if(this.feuilles[key]!=undefined&&this.feuilles[key]!='unload')
				if(this.feuilles[key] && this.feuilles[key].nom)
					{this.feuilles[key].hide();}
				}
			}
		else	{

			if(this.feuilles[nom] && this.feuilles[nom].nom)
				{this.feuilles[nom].hide();}
			}
		},
	write:function(f)
		{
		this.methodRun='Tclasseur:write';this.erreur=0;
		if(typeof(f)!='object'){this.erreur=-1;return -1;}

		var fe=this.feuilles[f.feuille];
		if(fe != undefined && fe != 'unload')
			{fe.write(f.texte);}
		}
	}



/* ************************************************************
instantiation du gestionnaire
 ***********************************************************/
if(typeof(gestLib)==='object')gestLib.end('gestClasseurs');

/*! - ./locales/void.js - */
