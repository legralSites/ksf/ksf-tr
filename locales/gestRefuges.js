function Trefuge_data(){
	this.p_id=0;
	this.id=0
	this.nom="";
	this.niveau=0;
	this.praxisId=0;
	this.code="";
	this.dateCreation="";
	this.occupationNb=0;
	this.description="";
	return this;
}

function TgestRefuge(c){
	this.datas=new Trefuge_data();
	this.setDatas(c);
	return this;
}

TgestRefuge.prototype={
	setDatas:function(dt){
		if(typeof(dt)!="object")return null;
		if(dt.p_id) this.setpId(dt.p_id);
		if(dt.ref_id) this.setId(dt.ref_id);
		if(dt.ref_nom)this.datas.nom=dt.ref_nom;
		if(dt.ref_niveau)this.datas.niveau=dt.ref_niveau;
		if(dt.ref_praxisId)this.setPraxisId(dt.ref_praxisId);
		if(dt.ref_code)this.datas.code=dt.ref_code;
		if(dt.ref_dateCreation)this.datas.dateCreation=dt.ref_dateCreation;
		if(dt.ref_occupationNb)this.datas.occupationNb=dt.ref_occupationNb;
		if(dt.ref_description)this.datas.description=dt.ref_description;
	return null;
	}
	,setpId:function(v) {
		if(!isNaN(v))
			this.datas.p_id=v;
		return null;
	}
	,setId:function(v) {if(!isNaN(v))this.datas.id=v;	return null;}
	,setPraxisId:function(v) {if(!isNaN(v))this.datas.praxisId=v;	return null;}

	/******************************
	 * fonction pour les clients
	 * le client ne gere qu'un joueur
	 * et gere l'affichage
	 *****************************/
	,show:function(){
		document.getElementById('ref_pid').innerHTML=gestPersonnages.personnages[this.datas.p_id].datas.nom+"("+this.datas.p_id+")";
		document.getElementById('ref_id').innerHTML=this.datas.id;
		document.getElementById('ref_nom').innerHTML=this.datas.nom;
		document.getElementById('ref_niveau').innerHTML=this.datas.niveau;
		document.getElementById('ref_praxisId').innerHTML=this.datas.praxisId;
		document.getElementById('ref_code').innerHTML=this.datas.code;
		document.getElementById('ref_dateCreation').innerHTML=this.datas.dateCreation;
		document.getElementById('ref_occupationNb').innerHTML=this.datas.occupationNb;
		document.getElementById('ref_description').innerHTML=this.datas.description;
	}


}// TgestRefuge.prototype=

/******************************
 * fonction pour le  serveur node
 * node appelle gestJoueurs
 * pour gerer et mettre en cache les joueurs 
 *****************************/
function TgestRefuges(){
	this.nb=0;
	this.selectNo=0;
	this.refuges=Array();
	return this;
}


TgestRefuges.prototype={

	// - ajoute un (seul) personnage p est un objet (et non un array)  - // 
	_add:function(c){
		var erreur=0;
		this.selectNo=c.ref_id;
		this.nb++;
		this.refuges[c.ref_id]= new TgestRefuge(c);

		// - erreur a definir: abs de nom? etc - //
		if(erreur!=0){
			this.refuges[c.id]=null;
			this.nb--;
		}
		return null;
	}

	// - ajoute un (objet) ou plusieurs (tableau d'objet) personnage(s) - //
	,add:function(liste){
		// - est ce un tableau d'object? - //
		if(typeof(liste[0].ref_id)=='number')
			for (var p in liste){
				if(liste[p]!=undefined)this._add(liste[p]);
			}
		// cet object ne contient pas de variable nomme 0
		else this._add(liste);
	}
	,getNomById:function(id){
		return this.refuges[id].datas.nom;
	}

	// --- fonctions specifiques au client --- //

	,show:function(IDHTML){
		//alert("a");
		if(!IDHTML)IDHTML='refuges-liste';
		var IDCSS=document.getElementById(IDHTML);
		if(IDCSS==undefined){
			return null;
		}

		IDCSS.innerHTML="";
		this.refuges[this.selectNo].show();
	}


}// TgestRefuge.prototype=

/******************************
 * fonction pour le  serveur node
 * node appelle gestJoueurs
 * pour gerer et mettre en cache les joueurs 
 *****************************/
function TgestRefuges(){
	this.nb=0;
	this.refuges=Array();
	return this;
}


TgestRefuges.prototype={

	// - ajoute un (seul) personnage p est un objet (et non un array)  - // 
	_add:function(c){
		var erreur=0;
		this.selectNo=c.ref_id;
		this.nb++;
		this.refuges[c.ref_id]= new TgestRefuge(c);

		// - erreur a definir: abs de nom? etc - //
		if(erreur!=0){
			this.refuges[c.id]=null;
			this.nb--;
		}
		return null;
	}

	// - ajoute un (objet) ou plusieurs (tableau d'objet) personnage(s) - //
	,add:function(liste){
		// - est ce un tableau d'object? - //
		if(typeof(liste[0].ref_id)=='number')
			for (var p in liste){
				if(liste[p]!=undefined)this._add(liste[p]);
			}
	}

	// --- fonctions specifiques au client --- //

	,show:function(IDHTML){
		//alert("a");
		if(!IDHTML)IDHTML='refuges-liste';
		var IDCSS=document.getElementById(IDHTML);
		if(IDCSS==undefined){
			return null;
		}

		IDCSS.innerHTML="";
		this.refuges[this.selectNo].show();
	}

	// - construit un menu deroulant avec les nom,prenom des personnages - //
	,menuListeBuild:function(){
		var out='<form name="ref_MenuListeForm">';
		out="";
		out+='<select id="ref_MenuListeSelect" name="ref_MenuListeSelect" onchange="gestRefuges.menuListeOnChange()">';
		out+="<option>refuge:</option>"
		for(var r in this.refuges){
			if(r==undefined)continue;
			out+="<option";
			if(r === this.selectNo)out+=' selected="selected"';
			out+=' value="'+r+'" >';
			dt=this.refuges[r].datas;
			out+=gestPersonnages.personnages[dt.p_id].datas.nom+", ("+dt.id+") "+dt.nom+"</option>";
		}
		out+="</select>";//</form>";
		return out;
	}

	,menuListeOnChange:function(){
		var IDCSS=document.getElementById("ref_MenuListeSelect");
		var selectedIndex=IDCSS.options.selectedIndex;
		this.selectNo=IDCSS.options[selectedIndex].value;
		//this.displayCarac();
		this.show();
	}


} // TgestRefuges.prototype=




// -- activer en tant que module node.js -- //
try{
	exports.TgestRefuge=TgestRefuge;
	exports.gestRefuge= new TgestRefuge();

	exports.TgestRefuges=TgestRefuges;
	exports.gestRefuges= new TgestRefuges();
	}catch(e){}

// - pour appeler en tant que module - //
// var gestJoueurPath=clientPath+'locales/gestJoueur.js';
// var gestJoueurLib =require(gestJoueurPath);

// -- pour creer une nouvelle instance -- //
//var plisE=new gestJoueurLib.TgestJoueur();


// - instencier en une ligne  (a verifier) - //
//var plisE=require(gestJoueurPath).gestJoueur();

