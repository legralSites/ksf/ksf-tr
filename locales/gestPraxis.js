function Tpraxis_data(){
	this.id=0;
	this.nom="";
	this.description="";
	return this;
}

function TgestPraxis(i){
	this.datas=new Tpraxis_data();
	this.setDatas(i);
	return this;
}

TgestPraxis.prototype={
	setDatas:function(dt){
		if(typeof(dt)!="object")return null;
		if(dt.id)		this.setId(dt.id);
		if(dt.nom)		this.datas.nom=dt.nom;
		if(dt.description)	this.datas.description=dt.description;

	return null;
	}
	,setId:function(v)		{if(!isNaN(v))this.datas.id=v;	return null;}

	/******************************
	 * fonction pour les clients
	 * le client ne gere qu'un joueur
	 * et gere l'affichage
	 *****************************/
	,show:function(){
		document.getElementById('pra_id').innerHTML=this.datas.id;
		document.getElementById('pra_nom').innerHTML=this.datas.nom;
		document.getElementById('pra_description').innerHTML=this.datas.description;
	}

}// TgestPraxis.prototype=


/******************************



/******************************
 * fonction pour le  serveur node
 * node appelle gestJoueurs
 * pour gerer et mettre en cache les joueurs 
 * attention au S de fin!!!!
 *****************************/
function TgestPraxiS(){
	this.nb=0;
	this.praxis=Array();
	return this;
		return this.clans[id].datas.nom;
	}

TgestPraxiS.prototype={

	getNomById:function(id){
		return this.praxis[id].datas.nom;
	}

	// --- fonctions specifiques au client --- //

	,showAll:function(IDHTML){
		//alert("a");
		if(!IDHTML)IDHTML='praxis-liste';
		var IDCSS=document.getElementById(IDHTML);
		if(IDCSS==undefined){
			return null;
		}

		IDCSS.innerHTML="";

		for(var praxis in this.praxis){
			var p=this.praxis[praxis].datas;
			if(p==undefined)continue;
			IDCSS.innerHTML+="<div>id:"+p.id+"</div>";
			IDCSS.innerHTML+="<div>nom:"+p.nom+"</div>";
			IDCSS.innerHTML+="<div>description:"+p.description+"</div>";
		}
	}

	// - ajoute un (seul) personnage p est un objet (et non un array)  - // 
	,_add:function(p){
		var erreur=0;
		this.nb++;
		this.praxis[p.id]= new TgestPraxis(p);

		// - erreur a definir: abs de nom? etc - //
		if(erreur){
			this.praxis[p.id]=null;
			this.nb--;
		}
	}

	// - ajoute un (objet) ou plusieurs (tableau d'objet) personnage(s) - //
	,add:function(liste){
		// - est ce un tableau d'object? - //
		if(typeof(liste[0]))
			for (var p in liste) this._add(liste[p]);
		// cet object ne contient pas de variable nomme 0
		else this._add(liste);
	}




} // TgestPraxiS.prototype


var gestPraxiS= new TgestPraxiS();
//	gestPraxiS.nb=1;
//	gestPraxiS.praxis[1].datas.id=1;
//	gestPraxiS.praxis[1].datas.nom="Dijon";
gestPraxiS._add({"id":1,"nom":"Dijon","description":"praxis de Dijon"})

	
// -- activer en tant que module node.js -- //
try{

	exports.TgestPraxis=TgestPraxis;
	//exports.gestPraxis= new TgestPraxis();
	exports.gestPraxis= new gestPraxis;

	exports.TgestPraxiS=TgestPraxiS;
	exports.gestPraxiS= new TgestPraxiS();

	}catch(e){}

// - pour appeler en tant que module - //
// var gestJoueurPath=clientPath+'locales/gestJoueur.js';
// var gestJoueurLib =require(gestJoueurPath);

// -- pour creer une nouvelle instance -- //
//var plisE=new gestJoueurLib.TgestJoueur();


// - instencier en une ligne  (a verifier) - //
//var plisE=require(gestJoueurPath).gestJoueur();



