/***************************************
 * loadPersonnages ( ByJoueuruuid() )
 * charge tous les personnages d'un joueur qui correspond a son uuid 
 * ***************************************/	
function loadPersonnages(){
	var prefixe="loadPersonnages() :";
	gestLib.write({"lib":"socketio","ALL":prefixe+"BEGIN"});
	var uuid=fjoueur.elements[0].value;	// OULA, c'est DANGEROUS ca!!!!
	var plis= new TgestPlis();
	plis.cmd='personnagesGet';
	plis.cmdReponse='personnagesSet';
	plis.j_uuid=uuid;
	plis.sqlProcNom='personnagesGetByj_uuid';

	gestLib.write({"lib":"socketio","ALL":plis.cmd+": chargement des personnages du joueur"});
	gestSocket.socket.emit(plis.cmd,plis);
	gestLib.write({"lib":"socketio","ALL":prefixe+plis.cmd+"END"});
}


function personnagesSet(gestSocket){
	// - ---------------------------------------- - -//
	// - reception des personnages relatif au joueurs - //
	gestSocket.socket.on('personnagesSet', function (plis) {
		var plis=JSON.parse(plis);
		gestLib.write({"lib":"socketio","ALL":plis.cmdReponse+":BEGIN"});

		var listeP=plis.sqlRows[0];
		gestPersonnages.add(listeP);

		// - creer les feuilles des personnages (ficher deja creer ET personnage unique - //
//		for (var pNu=0;pNu< gestPersonnages.Nb;pNu++){
//			classeurKSF.loadFeuille({"nom":"fiche-personnage"+pNu,"titre":"perso:"+gestPersonnages.personnages[pNu].datas.nom,"texte":"fiche du personnage:"+gestPersonnages.personnages[pNu].datas.nom});

		// - on charge la  page fiche-personnage - //
		plis= new TgestPlis({
			"page":"fiche-personnage"					// chargement de la page "fiche-personnage"
//			,"feuilleDest":"fiche-personnage"+pNu				// la pagesera afficher dasn cette feuille
			,"feuilleDest":"fiche-personnage"				// la pagesera afficher dasn cette feuille
		//	,diplayCaracPersoNo:pNu
		});	

		// - chargement de la page et affichage dans la feuille - //
		getPage(plis);

		// - on affiche les carac du perso  - //
//		gestPersonnages.personnages[pNu].displayCarac();

//		} // for


		// - charger les donnees dependant du personnage - //
		// - clans: on charge les donnees - //
//	loadRefuges();



// -  on revient sur la page du joeur - //
//		classeurKSF.select('fiche-joueur');

		gestLib.write({"lib":"socketio","ALL":plis.cmdReponse+":END"});
	});
} // personnagesSet
