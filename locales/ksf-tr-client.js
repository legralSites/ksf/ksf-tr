/*******************************
 * variables et objects généraux
 *
 *******************************/
/**
 * Retourne la valeur d'un paramètre d'une url
 * 
 * @param string param
 * nom du paramètre dont on souhaite avoir la valeur
 * @param url
 * url dans laquel on souhaite récupérer le paramètre ou rien si l'on souhaite travailler sur l'url courante
 * @return String
 * @author Labsmedia
 * @see http://www.labsmedia.com
 * @licence GPL
 * exemple: http://dev.petitchevalroux.net/javascript/recuperer-parametre-dans-une-url-javascript.140.html
 */
function getParamValue(param,url){
        var u = url == undefined ? document.location.href : url;
        var reg = new RegExp('(\\?|&|^)'+param+'=(.*?)(&|$)');
        matches = u.match(reg);
        return (matches && matches[2]) ? decodeURIComponent(matches[2]).replace(/\+/g,' ') : '';
}

// - connexion au serveur - //
var DEV=getParamValue('DEV');
var RPI=getParamValue('RPI');
var LAN=getParamValue('LAN');

var serveurio={"IP":"mediatheques.legral.fr","PORT":33081};
if(RPI) serveurio.IP="192.168.100.253";
if(LAN) serveurio.IP="192.168.100.147";
if(DEV) serveurio.IP="127.0.0.1";

serveurio.URL="http://"+serveurio.IP+":"+serveurio.PORT;

var cookie= new Tcookies();

//var plis=null;	// recepteur d'instance temporaire

// - gestion des spheres - //
var gestSpheres=new TgestSpheres();

// - gestion des Zones - //
//var gestZones=new TgestZones();

// - gestion du joueur - //
var gestJoueur=new TgestJoueur();

// - gestion des personnages - //
var gestPersonnages=new TgestPersonnages();

// - gestion des clans - //
var gestClans=new TgestClans();

// - gestion des clans - //
var gestRefuges=new TgestRefuges();

// - gestion des clans - //
//var gestZones=new TgestZones();

/*******************************
 * fonctions generique
 *
 *******************************/
// - surcharge l'object 1 avec les donnees de l'object 2 - //
// l'object 1 est modifie
function surchargeObj(obj1,obj2){
	if(typeof(obj1)!="object")return 0;
	if(typeof(obj2)!="object")return 0;
	for(var i in obj2) obj1[i]=obj2[i];
	return 1;
}

String.prototype.capitalize=function(){
	return this.charAt(0).toUpperCase()+this.slice(1);
}


function cookieSetuuid(){
	date=new Date();
	date.setMonth(date.getMonth()+12);
	uuid=document.forms.fjoueur[0].value;	// mm pb que pour loadGame!!!!

	cookie.set("ksf-joueur-uuid", uuid,date);
}



/***************************************
 * getPage
 * demande une page au serveur
 ***************************************/
function getPage(plis){
	var prefixe="getPage(plis): ";
	gestLib.write({"lib":"socketio","ALL":prefixe+"BEGIN"});
	if (!plis.page){
		gestLib.write({"lib":"socketio","ALL":prefixe+"ERREUR: plis.page n'est pas indiquer!"});
		return null;
	}
	plis.cmd='getPage';
	plis.cmdReponse='setPage';
	gestLib.write({"lib":"socketio","ALL":prefixe+"getPage(plis): plis.page:"+plis.page});

	gestSocket.socket.emit(plis.cmd,plis);
	gestLib.write({"lib":"socketio","ALL":prefixe+"END"});
}

/***************************************
 * loadGame()
 * charge toutes les infos du jeu 
 * qui conerne le joueur correspondant au uuid
 ***************************************/	
function loadGame(){
	var prefixe="loadGame(): ";
	gestLib.write({"lib":"socketio","ALL":prefixe+"BEGIN"});
	var uuid=fjoueur.elements[0].value;	// OULA, c'est DANGEROUS ca!!!!
	var plis= new TgestPlis();
	plis.cmd='getGame'; // demande des infos sur un joeur (en vue de sa connexion, nom a changer en joueurGetByuuid)
	plis.cmdReponse='setGame';
	plis.j_uuid=uuid;
	plis.sqlProcNom='joueurGetByuuid';

	gestLib.write({"lib":"socketio","ALL":prefixe+" connexion du joueur avec l'uuid:"+plis.j_uuid});
	gestSocket.socket.emit(plis.cmd,plis);
}


/***************************************
 *
 ***************************************/
var gestSocket=null; // preparativon a l'instanciation


function TgestSocket(){
	var prefixe="TgestSocket(): ";
	gestLib.write({"lib":"gestLib","ALL":prefixe+"connexion à "+serveurio.URL});
	this.socket = io(serveurio.URL);


	// - ---------------------------------------- - -//
	// - receptionModele - //
	this.socket.on('receptionModele', function (plis) {
//		ioAnimate.ioDwShow();
		var plis=JSON.parse(plis);
		gestLib.write({"lib":"socketio","ALL":plis.cmdReponse+":BEGIN"});

//		if(plis_isErreur(plis))return null;


		gestLib.write({"lib":"socketio","ALL":plis.cmdReponse+":END"});
	});


	// - ---------------------------------------- - -//
	// - reception de donnees brutes sans contexte - //
	this.socket.on('setDatas', function (data) {
//		ioAnimate.ioDwShow();
		var plis=JSON.parse(plis);
		gestLib.write({"lib":"socketio","ALL":plis.cmdReponse+":BEGIN"});

//		if(plis_isErreur(plis))return null;


		gestLib.write({"lib":"socketio","ALL":plis.cmdReponse+":END"});
	});


	// - reception de données - //


	// - ---------------------------------------- - -//
	// - reception initial ou d'une maj d'une page - //
	// le contenu sera ecrit dans une feuille (feuilleDest)
	this.socket.on('setPage', function (plis) {
		//ioAnimate.ioDwShow();
		var plis=JSON.parse(plis);
		gestLib.write({"lib":"socketio","ALL":plis.cmdReponse+"reception d'une page:"+plis.page});

	//	if(plis_isErreur(plis))return null;


		// - on ecrit dans la feuille le contnu de la page - //
		classeurKSF.write({"feuille":plis.feuilleDest,"texte":plis.content});

		
		// - actions a faire a la recpetion d'une page precise - //
		switch(plis.page){
			case'fiche-clans':
				classeurKSF.feuilles[plis.page].ongletIDCSS.innerHTML=classeurKSF.feuilles[plis.page].ongletIDCSS.innerHTML.capitalize();
				gestClans.showAll();
				break;
			case 'fiche-spheres':
				// - changer le nom de la feuille - //
				classeurKSF.feuilles[plis.page].ongletIDCSS.innerHTML=classeurKSF.feuilles[plis.page].ongletIDCSS.innerHTML.capitalize();
				gestSpheres.showAll();
				break;
			case 'fiche-personnage':
				// - changer le nom de la feuille - //
				//classeurKSF.feuilles[plis.page].ongletIDCSS.innerHTML=classeurKSF.feuilles[plis.page].ongletIDCSS.innerHTML.capitalize();
				classeurKSF.feuilles[plis.page].ongletIDCSS.innerHTML=gestPersonnages.menuListeBuild();
				gestPersonnages.displayCarac();
				
				// -- charger les donnees dependant du personnage -- //
				// --- clans: on charge les donnees --- //
				loadRefuges();

				break;
			case 'fiche-refuges':
				// - changer le nom de la feuille - //
				//classeurKSF.feuilles[plis.page].ongletIDCSS.innerHTML=classeurKSF.feuilles[plis.page].ongletIDCSS.innerHTML.capitalize();
				classeurKSF.feuilles[plis.page].ongletIDCSS.innerHTML=gestRefuges.menuListeBuild();
				gestRefuges.show(); // on affiche que le 1er meme si y en a d'autre
				break;
			case 'fiche-admins':
				// - changer le nom de la feuille - //
//				classeurKSF.feuilles[plis.page].ongletIDCSS.innerHTML=classeurKSF.feuilles[plis.page].ongletIDCSS.innerHTML.capitalize();
				break;
		}
		// - actons a faire a la reception d'une valeur precise - //

	});

	// - ---------------------------------------- - -//
	// - reception de la reponse d'une demande de connection d'un joueur - //
	this.socket.on('setGame', function (plis) {
		var plis=JSON.parse(plis);
		//ioAnimate.ioDwShow();
		gestLib.write({"lib":"socketio","ALL":plis.cmdReponse+":"});

//		if(plis_isErreur(plis))return null;

		// - ananlyse de la reponse sql - //
		// y a til des donnees?
		var sqlValues=plis.sqlRows[0];
		if(sqlValues.length==0){
			gestLib.write({"lib":"socketio","ALL":"cmd: "+plis.cmdReponse+": aucun joueur n'a cet uuid" });
			return null;
		}
		// - donnees presente - //
		gestLib.write({"lib":"socketio","ALL":"cmd: "+plis.cmdReponse+": joueur trouver" });
		gestJoueur.isConnect=1;
		gestJoueur.setDatas(sqlValues[0]); // il n'a qu'un seul joueur

		// - afficher les infos du joueurs - //
		gestJoueur.show();

		// - charger les  personnages - //
		loadPersonnages();	// async!
		
		// - loadAdmins - //
		//loadAdmins();
		// - on charge la  page fiche-admins - //
		plis= new TgestPlis({
			"page":"fiche-admins"			// chargement de la page "fiche-personnage"
			,"feuilleDest":"fiche-admins"		// la pagesera afficher dasn cette feuille
		});	

		// - chargement de la page et affichage dans la feuille - //
		getPage(plis);


		// -  intialisation complete: le joueur est connecté - //
		gestJoueur.isConnect=1;

		//		classeurKSF.write({"feuille":plisR.page,"texte":plisR.content});
	});

	spheresSet(this);
	clansSet(this);
	personnagesSet(this);
	refugesSet(this);



return this;
} // gestSocket()



function display_switchBG(){
	var iDCsS=document.getElementById('p_BG').style;
       	iDCsS.display=(iDCsS.display=='none')?'block':'none';
}




/***************************************
 * main()
 * doit etre appeller une fois la page charge
 ***************************************/	
function main(){
//	ioAnimate=new TioAnimate();
//	ioAnimate.setIdCSS("ioUp","ioDw");
//	ioAnimate.check();

	// - console de log/debug - //
	gestLib.setConsole('jscolor');
	gestLib.setConsole('gestClasseurs');
	gestLib.setConsole('socketio');

	// - connexion au serveur - //
	gestSocket=new TgestSocket();

	// - classeur - //
	classeurKSF=new Tclasseur({"nom":"classeurKSF"});
	var plis=null;

	// - joueur: on charge la feuille - //
	classeurKSF.loadFeuille({"nom":"fiche-joueur","titre":"joueur","texte":"fiche perso du joueur"});
	// - on charge la page qui sera sur la feuille - //
	plis= new TgestPlis({"page":"fiche-joueur","feuilleDest":"fiche-joueur"});	getPage(plis);


	// - info: on charge la feuille - //
	classeurKSF.loadFeuille({"nom":"info","titre":"info","texte":"info en temps réel"});

	// - spheres: on charge la feuille - //
	classeurKSF.loadFeuille({"nom":"fiche-spheres","titre":"spheres","texte":"fiche des spheres"});
	// - spheres: on charge les donnees - //
	loadSpheres();

	// - clans: on charge la feuille - //
	classeurKSF.loadFeuille({"nom":"fiche-clans","titre":"clans","texte":"fiche des clans"});
	// - clans: on charge les donnees - //
	loadClans();

	// - personnage: on charge LA feuille (gestion multipersonnage desactiver!!!!) - //
	classeurKSF.loadFeuille({"nom":"fiche-personnage","titre":"personnage","texte":"fiche du personnage"});
	// - clans: on charge les donnees - //
	//loadPersonnage(); // impossible ici faut l'uuid du joueur

	// - refuges: on charge la feuille - //
	classeurKSF.loadFeuille({"nom":"fiche-refuges","titre":"refuges","texte":"fiche des refuges"});
	// - clans: on charge les donnees - //
	//loadRefuges(); // neccessite que les personnages soient charges

	// - admins: on charge la feuille - //
	classeurKSF.loadFeuille({"nom":"fiche-admins","titre":"administrations","texte":"commande d'administration"});

	// - - //
	classeurKSF.select('fiche-joueur');


	// - test d'erreur- //
//	plis= new TgestPlis({"page":"fiche-inexistante"});	getPage(plis);

	
	// - envoie d'une demande de toutes les infos concernant le personnage - //

	//emit_getPersoAll();


	// -a activer l'affichage d'un menu - //


	/* chargement de l'uuid dans les cookies */
	var uuid=cookie.get("ksf-joueur-uuid");
	document.forms.fjoueur[0].value=uuid;
//	document.getElementById('j_uuid').value=uuid;


}
