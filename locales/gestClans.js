function Tclan_data(){
	this.id=0
	this.nom='';
	this.description="";
	return this;
}

function TgestClan(c){
	this.datas=new Tclan_data();
	this.setDatas(c);
	return this;
}

TgestClan.prototype={
	setDatas:function(dt){
		if(typeof(dt)!="object")return null;
		if(dt.id) this.setId(dt.id);
		if(dt.nom)this.datas.nom=dt.nom;
		if(dt.description)this.datas.description=dt.description;
	return null;
	}
	,setId:function(v) {if(!isNaN(v))this.datas.id=v;	return null;}

	/******************************
	 * fonction pour les clients
	 * le client ne gere qu'un joueur
	 * et gere l'affichage
	 *****************************/
	,show:function(){
/*		document.getElementById('j_id').innerHTML=this.datas.j_id;
		document.getElementById('j_nom').innerHTML=this.datas.j_nom;
		document.getElementById('j_prenom').innerHTML=this.datas.j_prenom;
		document.getElementById('j_mail').innerHTML=this.datas.j_mail;
		document.getElementById('j_note').value=this.datas.j_note;
*/
	}

}// TgestClan.prototype=

/******************************
 * fonction pour le  serveur node
 * node appelle gestJoueurs
 * pour gerer et mettre en cache les joueurs 
 *****************************/
function TgestClans(){
	this.nb=0;
	this.clans=Array();
	return this;
}


TgestClans.prototype={

	// - ajoute un (seul) personnage p est un objet (et non un array)  - // 
	_add:function(c){
		var erreur=0;
		//var clanNu=this.nb++;
		this.nb++;
		this.clans[c.id]= new TgestClan(c);

		// - erreur a definir: abs de nom? etc - //
		if(erreur!=0){
			this.clans[c.id]=null;
			this.nb--;
		}
		return null;
	}

	// - ajoute un (objet) ou plusieurs (tableau d'objet) personnage(s) - //
	,add:function(liste){
		// - est ce un tableau d'object? - //
		if(typeof(liste[0].id)=='number')
			for (var p in liste) this._add(liste[p]);
		// cet object ne contient pas de variable nomme 0
		else this._add(liste);
	}
	,getNomById:function(id){
		return this.clans[id].datas.nom;
	}

	// --- fonctions specifiques au client --- //

	,showAll:function(IDHTML){
		//alert("a");
		if(!IDHTML)IDHTML='clans-liste';
		var IDCSS=document.getElementById(IDHTML);
		if(IDCSS==undefined){
			return null;
		}

		IDCSS.innerHTML="";

		for(var clan in this.clans){
			if(clan==undefined)continue;
			var c=this.clans[clan].datas;
			if(c==undefined)continue;
			IDCSS.innerHTML+="<div>"+c.nom+"</div>";
			IDCSS.innerHTML+="<div>"+c.description+"-</div>";
		}
	}
}




// -- activer en tant que module node.js -- //
try{
	exports.TgestClan=TgestClan;
	exports.gestClan= new TgestClan();

	exports.TgestClans=TgestClans;
	exports.gestClans= new TgestClans();
	}catch(e){}

// - pour appeler en tant que module - //
// var gestJoueurPath=clientPath+'locales/gestJoueur.js';
// var gestJoueurLib =require(gestJoueurPath);

// -- pour creer une nouvelle instance -- //
//var plisE=new gestJoueurLib.TgestJoueur();


// - instencier en une ligne  (a verifier) - //
//var plisE=require(gestJoueurPath).gestJoueur();

