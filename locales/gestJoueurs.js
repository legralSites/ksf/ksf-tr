function Tjoueur_data(){
	this.j_uuid=this.j_id=0;
	this.j_nom=this.j_prenom=this.j_note="";
	this.personnages_uuid=[];	// liste des uuid des personnages que le joueur controle

	return this;
}

function TgestJoueur(i){
	this.datas=new Tjoueur_data();
	this.setDatas(i);
	this.gestPersonnages=new TgestPersonnages();
	return this;
}

TgestJoueur.prototype={
	isConnect:0
	,setDatas:function(dt){
		if(typeof(dt)!="object")return null;
		if(dt.j_id)		this.setId(dt.j_id);

		if(dt.j_nom)		this.setNom(dt.j_nom);
		if(dt.j_prenom)		this.setPrenom(dt.j_prenom);

		if(dt.j_mail)		this.datas.j_mail=dt.j_mail;
		if(dt.j_note)		this.datas.j_note=dt.j_note;

	return null;
	}
	,setId:function(v)		{if(!isNaN(v))this.datas.j_id=v;	return null;}
	,setNom:function(v)		{if(v!="")this.datas.j_nom=v;		return null;}
	,setPrenom:function(v)		{if(v!="")this.datas.j_prenom=v;	return null;}
	,setNote:function(v)		{if(v!="")this.datas.j_note=v;		return null;}

	/******************************
	 * fonction pour les clients
	 * le client ne gere qu'un joueur
	 * et gere l'affichage
	 *****************************/
	,show:function(){
		document.getElementById('j_id').innerHTML=this.datas.j_id;
		document.getElementById('j_nom').innerHTML=this.datas.j_nom;
		document.getElementById('j_prenom').innerHTML=this.datas.j_prenom;
		document.getElementById('j_mail').innerHTML=this.datas.j_mail;
		document.getElementById('j_note').value=this.datas.j_note;
	}

}// Tgestjoueur.prototype=


/******************************



/******************************
 * fonction pour le  serveur node
 * node appelle gestJoueurs
 * pour gerer et mettre en cache les joueurs 
 *****************************/
function TgestJoueurs(){
	this.joueurs=Array();
	return this;
}


// -- activer en tant que module node.js -- //
try{
	exports.TgestJoueur=TgestJoueur;
	exports.gestJoueur= new TgestJoueur();
	}catch(e){}

// - pour appeler en tant que module - //
// var gestJoueurPath=clientPath+'locales/gestJoueur.js';
// var gestJoueurLib =require(gestJoueurPath);

// -- pour creer une nouvelle instance -- //
//var plisE=new gestJoueurLib.TgestJoueur();


// - instencier en une ligne  (a verifier) - //
//var plisE=require(gestJoueurPath).gestJoueur();

