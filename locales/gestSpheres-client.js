/***************************************
 * loadSpheres () )
 * charge toutes les spheres 
 * ***************************************/	
function loadSpheres(){
	var prefixe="loadSpheres() :";
	gestLib.write({"lib":"socketio","ALL":prefixe+"BEGIN"});
	var plis= new TgestPlis();
	plis.cmd='spheresGet';
	plis.cmdReponse='spheresSet';
//	plis.j_uuid=uuid;
	plis.sqlProcNom=plis.cmd;

	gestLib.write({"lib":"socketio","ALL":plis.cmd+": chargement des spheres"});
	gestSocket.socket.emit(plis.cmd,plis);
	gestLib.write({"lib":"socketio","ALL":prefixe+plis.cmd+"END"});
}


function spheresSet(gestSocket){
// - ---------------------------------------- - -//
	// - reception des spheres - //
	gestSocket.socket.on('spheresSet', function (plis) {
		var prefixe=plis.cmdReponse+": ";
		var plis=JSON.parse(plis);
		gestLib.write({"lib":"socketio","ALL":plis.cmdReponse+":BEGIN"});
	
//		if(plis_isErreur(plis))return null;

		var liste=plis.sqlRows[0];
		gestSpheres.add(liste);

		// - creer la feuille des spheres (deja exitante -> la rendre visible - //
		// classeurKSF.loadFeuille({"nom":"fiche-spheres","titre":"spheres","texte":"fiche des spheres:"});

		// - afficher l'indicateur de nouveau contenu - //
		//classeurKSF.feuilles[plis.page].ongletIDCSS.innerHTML=classeurKSF.feuilles[plis.page].ongletIDCSS.innerHTML.capitalize();

		// - on charge la  page fiche-spheres - //
		plis= new TgestPlis({
			"page":"fiche-spheres"					// chargement de la page "fiche-personnage"
			,"feuilleDest":"fiche-spheres"				// la page sera afficher dans cette feuille
		});	

		// - chargement de la page et affichage dans la feuille - //
		getPage(plis);
		
		// -  on selectionne la feuille  - //
		//classeurKSF.select('fiche-joueur');

		gestLib.write({"lib":"socketio","ALL":prefixe+plis.cmdReponse+":END"});
	});
} // function spheresSet()
