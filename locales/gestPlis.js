/************************
 * TgestPlis()
 * objet de donnees pour standardisé les envoies de donnees
 * (notemment avec socketio)
 ************************/

if(typeof(gestLib)==='object')	gestLib.loadLib({nom:'TgestPlis',ver:"0.0.1",description:'object pour standartise les envoie de données'});else gestLib=null;

var gestPlisERREUR={"E_SQL":1,"W_SQL":-1,"E_SOCKETIO":2,"W_SOCKETIO":-2};



var TgestPlisErreur=function(){
	this.errno=0;
	this.txt="";
	this.sql= {"errno":0,"txt":""};
	this.socketio={"errno":0,"code":""};//format renvoyer par socketio
	this.essai=1;
}


function TgestPlis(plis){
	var p=(typeof(plis)=='object')?plis:{};
//	this.erreurNo=(!isNaN(p.erreurNo))?p.erreurNo:0;
	this.err=new TgestPlisErreur();
//	this.erreurNo=0;
//	this.erreurTxt='';
//	this.eNode=null;		// erreur/exeption de node(object)
	this.nom='';			// nom du plis (ou de la donnee)
	this.page='';
	this.feuilleDest='';		// feuille de destination ou sera ecrit le contnu de 'page'
	this.cmd='';			// commande qui doit etre executer a reception
	this.cmdReponse='';		// commande qui Recoit le plis (peut etre fournit par le plis initial(plisE: 1er envoie) pour indiquer par quelle commande elle doit etre recut)

	this.sqlProcNom='';		// nom de la procedure sql stocke qui doit etre appeller  
	this.sqlQuery=''
//	this.sqlResult='';
//	this.sqlErreur='';
	this.socket=null;		// socket utiliser pour l'echange TR
	this.cb={};
	this.content='';		// contenu de fichier
	// - surcharge des param standart ou ajout de nouveau - //
	for (elt in plis) this[elt]=plis[elt];
	if(this.cmdReponse=='')this.cmdReponse=this.cmd;	// si la commande de retour n'est pas indique alors c'est la meme que la commande d'envoie
	return this;

}

/*********************
 * emit(plis)
 * envoie le plis
 * detruit la socket avant envoie pour ne pas l'envoyer (pb ref cyclique)
 *********************/

function Trepondre(plis){
//	t="plis.socket="+typeof(plis.socket);console.log(t.data);
//	console.log(typeof(plis.socket));
	var sock=plis.socket;

//	t="Asock="+typeof(sock);console.log(t.data);console.dir(sock);

	plis.socket=null;
//	t="Bsock="+typeof(sock);console.log(t.data);console.dir(sock);
	sock.emit(plis.cmdReponse,JSON.stringify(plis));
	return this;
}


/*********************
 * emit(plis)
 * envoie le plis
 * detruit la socket avant envoie pour ne pas l'envoyer (pb ref cyclique)
 *********************/
/*
function plis_erreurType(plis){
	var prefixe='plis_erreurType(plis): '+plis.cmdReponse+': ';
	gestLib.write({"lib":"socketio","ALL":prefixe+plis.cmdReponse+"BEGIN"});
	plis.err.errno=0;

	// - warn < 0 : n'est pas forcement une erreur - //
	if(!plis.sqlRows){
		plis.err.errno=W_SQL;
		plis.err.txt="sql: aucune donnée retournée!";

	}

	// - erreur >0 - //
	if(plis.err.sql.errno){
		plis.err.errno=E_SQL;
		plis.err.txt="
//		gestLib.write({"lib":"socketio","ALL":prefixe+"<span style='color:red;'>erreur sql ErreurNo: "+plis.erreurNo+":"+plis.erreurTxt+" sqlErreur no: "+plis.sqlErreur.errno+" : "+plis.sqlErreur.code+"</span>"});
	}

 
	if(plis.err.socketio.errno){
		plis.err.errno=E_SOCKETIO;
//       		gestLib.write({"lib":"socketio","ALL":prefixe+"<span style='color:red;'>erreurNode No: "+plis.eNode.errno+" : "+plis.eNode.code+"</span>"});
	}
	gestLib.write({"lib":"socketio","ALL":prefixe+"END"});
	return err;
}
*/

/* ************************************************************
   ***********************************************************/
 // .end fait planter node
if(gestLib)gestLib.end('TgestPlis');

// - exporter en module nodejs - //
// attention envoie la fonction et nom une instance
try{
	exports.erreur=TgestPlisErreur;
	exports.TgestPlis=TgestPlis;
	exports.gestPlis= new TgestPlis();
	exports.repondre=Trepondre;
	exports.isErreur=TisErreur;

}catch(e){}

// - pour appeler en tant que module - //
// var gestPlisPath=clientPath+'locales/gestPlis.js';
// var gestPlisLib =require(gestPlisPath);

// -- pour creer une nouvelle instance -- //
//var plisE=new gestPlisLib.TgestPlis();


// - instencier en une ligne  (a verifier) - //
//var plisE=require(gestPlisPath).gestPlis();

 
