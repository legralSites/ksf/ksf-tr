/***************************************
 * loadClans () )
 * charge tous les clans
 * ***************************************/	
function loadClans(){
	var prefixe="loadClans() :";
	gestLib.write({"lib":"socketio","ALL":prefixe+"BEGIN"});
	var plis= new TgestPlis();
	plis.cmd='clansGet';
	plis.cmdReponse='clansSet';
//	plis.j_uuid=uuid;
	plis.sqlProcNom=plis.cmd;

	gestLib.write({"lib":"socketio","ALL":plis.cmd+": chargement des clans"});
	gestSocket.socket.emit(plis.cmd,plis);
	gestLib.write({"lib":"socketio","ALL":prefixe+plis.cmd+"END"});
}

	// - ---------------------------------------- - -//
	// - reception des clans - //
	// charge la fiche
	// et affiches  les donnees dans la fiche - //
function clansSet(gestSocket){
	gestSocket.socket.on('clansSet', function (plis) {
		var plis=JSON.parse(plis);
		gestLib.write({"lib":"socketio","ALL":plis.cmdReponse+":BEGIN"});

//		if(plis_isErreur(plis))return null;

		var liste=plis.sqlRows[0];
		gestClans.add(liste);

		// - creer la feuille des clans (deja exitante -> la rendre visible - //
		// classeurKSF.loadFeuille({"nom":"fiche-clan"+pNu,"titre":"pNu].datas.nom,"texte":"fiche du personnage:"+gestPersonnages.personnages[pNu].datas.nom});

		// - on charge la  page fiche-personnage - //
		plis= new TgestPlis({
			"page":"fiche-clans"					// chargement de la page "fiche-personnage"
			,"feuilleDest":"fiche-clans"				// la page sera afficher dans cette feuille
		});	

		// - rendre conditionnelle le chargement de la page - //
		// sinon la page sera charger a chaque mise a jours des donnees
		// - chargement de la page et affichage dans la feuille - //
		getPage(plis);
		
		// -  on revient sur la page du joeur - //
		//classeurKSF.select('fiche-joueur');

		gestLib.write({"lib":"socketio","ALL":plis.cmdReponse+":END"});
	});
} // clansSet()
