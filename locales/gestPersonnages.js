
/********************
 *
 ********************/

function personnage_data(){
	this.id=0;
	this.uuid=null;
	this.nom="";
	this.prenom="";
	this.joueurId=0;
	this.clanId=0;this.praxisId=0;this.factionId=0;
	this.XP_cumul=0;this.XP_dispo=0;
	this.attaque=0;this.defense=0;	this.initiative=0;
	this.efficacite=0;this.etendue=0;this.loyaute=0;
	this.BG=this.BG="";
	this.description=this.description="";
	return this;
}


/********************
 *
 ********************/

function TgestPersonnage(p){
	this.datas=new personnage_data();
	this.setDatas(p);
	return this;
}

TgestPersonnage.prototype={
	setDatas:function(dt){
		if(typeof(dt)!="object")return null;
		if(dt.p_id)		this.setId(dt.p_id);

		if(dt.p_uuid)		this.datas.uuid=dt.p_uuid;

		if(dt.p_nom)		this.setNom(dt.p_nom);
		if(dt.p_prenom)		this.setPrenom(dt.p_prenom);
		if(dt.p_joueurId)	this.setJoueurId(dt.p_joueurId);
		if(dt.p_clanId)		this.setClanId(dt.p_clanId);
		if(dt.p_praxisId)	this.setPraxisId(dt.p_praxisId);
		if(dt.p_factionId)	this.setFactionId(dt.p_factionId);

		if(dt.p_XP_cumul)	this.setXP_cumul(dt.p_XP_cumul);
		if(dt.p_XP_dispo)	this.setXP_dispo(dt.p_XP_dispo);

		if(dt.p_attaque)	this.setAttaque(dt.p_attaque);
		if(dt.p_defense)	this.setDefense(dt.p_defense);
		if(dt.p_initiative)	this.setInitiative(dt.p_initiative);

		if(dt.p_efficacite)	this.setEfficacite(dt.p_efficacite);
		if(dt.p_etendue)	this.setEtendue(dt.p_etendue);
		if(dt.p_loyaute)	this.setLoyaute(dt.p_loyaute);

		if(dt.p_BG)		this.setBG(dt.p_BG);
		if(dt.p_description)	this.setDescription(dt.p_description);
	return null;
	}
	,setId:function(v)		{if(!isNaN(v))this.datas.id=v;				return null;}
	,setNom:function(v)		{if(v!="")this.datas.nom=v;				return null;}
	,setPrenom:function(v)		{if(v!="")this.datas.prenom=v;				return null;}
	,setJoueurId:function(v)	{if(!isNaN(v))this.datas.joueurId=v;			return null;}
	,setClanId:function(v)		{if(!isNaN(v))this.datas.clanId=v;			return null;}
	,setPraxisId:function(v)	{if(!isNaN(v))this.datas.praxisId=v;			return null;}
	,setFactionId:function(v)	{if(!isNaN(v))this.datas.factionId=v;			return null;}

	,setXP_cumul:function(v)	{if(!isNaN(v))this.datas.XP_cumul=v;			return null;}
	,setXP_dispo:function(v) 	{if(!isNaN(v))this.datas.XP_dispo=v;			return null;}

	,setAttaque:function(v)		{if(!isNaN(v) && (v<4))this.datas.attaque=v;		return null;}
	,setDefense:function(v)		{if(!isNaN(v) && (v<4))this.datas.defense=v;		return null;}
	,setInitiative:function(v)	{if(!isNaN(v) && (v<4))this.datas.initiative=v;		return null;}

	,setEfficacite:function(v)	{if(!isNaN(v) && (v<4))this.datas.efficacite=v;		return null;}
	,setEtendue:function(v)		{if(!isNaN(v) && (v<4))this.datas.etendue=v;		return null;}
	,setLoyaute:function(v)		{if(!isNaN(v) && (v<4))this.datas.loyaute=v;		return null;}

	,setBG:function(v)		{if(v!="")this.datas.BG=v;				return null;}
	,setDescription:function(v)	{if(v!="")this.datas.description=v;			return null;}


	,displayCarac:function(){
		var idCSS=null;
		document.getElementById("p_id").innerHTML=this.datas.id;
		document.getElementById("p_uuid").innerHTML=this.datas.uuid;
		document.getElementById("p_nom").innerHTML=this.datas.nom;
		document.getElementById("p_prenom").innerHTML=this.datas.prenom;
		document.getElementById("p_nom").innerHTML=this.datas.joueurId;	// calculer
		document.getElementById("p_clanNom").innerHTML=this.datas.clanId+": "+gestClans.getNomById(this.datas.clanId);	// calculer
		document.getElementById("p_praxisNom").innerHTML=this.datas.praxisId+": "+gestPraxiS.getNomById(this.datas.praxisId);	// calculer
		document.getElementById("p_factionNom").innerHTML=this.datas.factionId+": "+gestClans.getNomById(this.datas.clanId);;

		document.getElementById("p_XP_cumul").innerHTML=this.datas.XP_cumul;
		document.getElementById("p_XP_dispo").innerHTML=this.datas.XP_dispo;
	
		document.getElementById("p_attaque").innerHTML=this.datas.attaque;
		document.getElementById("p_defense").innerHTML=this.datas.defense;
		document.getElementById("p_initiative").innerHTML=this.datas.initiative;
	
		document.getElementById("p_efficacite").innerHTML=this.datas.efficacite;
		document.getElementById("p_etendue").innerHTML=this.datas.etendue;
		document.getElementById("p_loyaute").innerHTML=this.datas.loyaute;

		document.getElementById("p_BG").innerHTML=this.datas.BG;
		document.getElementById("p_descriptionEdit").value=this.datas.description;
		document.getElementById("p_descriptionValue").innerHTML=this.datas.description;
	}
}// TgestPersonnage.prototype=

/********************
 *
 ********************/
function TgestPersonnages(){
	this.selectNo=Array();	// personnage selectionne (en cours)
	this.personnages=Array();
	this.Nb=0;

	return this;
}

TgestPersonnages.prototype={

	// - ajoute un (seul) personnage p est un objet (et non un array)  - // 
	_add:function(p){
		var erreur=0;
		var persoNu=p.p_id;
		this.Nb++;
		this.selectNo=persoNu;	// devient le personnage selectionne
		this.personnages[persoNu]= new TgestPersonnage(p);

		// - erreur a definir: abs de nom? etc - //
		if(erreur){
			this.personnages[persoNu]=null;
			this.Nb--;
		}
	}

	// - ajoute un (objet) ou plusieurs (tableau d'objet) personnage(s) - //
	,add:function(listeP){
		// - est ce un tableau d'object? - //
		if(typeof(listeP))
			for (var p in listeP) this._add(listeP[p]);
		// cet object ne contient pas de variable nomme 0
		else this._add(listeP);
	}

	,displayCarac:function(){
		this.personnages[this.selectNo].displayCarac();
	}

	// - construit un menu deroulant avec les nom,prenom des personnages - //
	,menuListeBuild:function(){
		var out='<form name="p_MenuListeForm">';
		out="";
		out+='<select id="p_MenuListeSelect" name="p_MenuListeSelect" onchange="gestPersonnages.menuListeOnChange()">';
		out+="<option>personnage:</option>"
		for(var p in this.personnages){
			if(p==undefined)continue;
			out+="<option";
			if(p === this.selectNo)out+=' selected="selected"';
			out+=' value="'+p+'" >';
			out+=this.personnages[p].datas.nom+","+this.personnages[p].datas.prenom+"</option>";
		}
		out+="</select>";//</form>";
		return out;
	}

	,menuListeOnChange:function(){
		var IDCSS=document.getElementById("p_MenuListeSelect");
		//this.selectNo=IDCSS.
		//this.selectedIndex=document.forms.p_MenuListeForm.p_MenuListeSelect.options.selectedIndex;
		var selectedIndex=IDCSS.options.selectedIndex;
		this.selectNo=IDCSS.options[selectedIndex].value;
		this.displayCarac();
	}

}


// -- activer en tant que module node.js -- //
try{
	exports.TgestPersonnage=TgestPersonnage;
	exports.gestPersonnage= new TgestPersonnage();

	exports.TgestPersonnages=TgestPersonnages;
	exports.gestPersonnages= new TgestPersonnages();
	}catch(e){}

// - pour appeler en tant que module - //
// var gestJoueurPath=clientPath+'locales/gestJoueur.js';
// var gestJoueurLib =require(gestJoueurPath);

// -- pour creer une nouvelle instance -- //
//var plisE=new gestJoueurLib.TgestJoueur();


// - instencier en une ligne  (a verifier) - //
//var plisE=require(gestJoueurPath).gestJoueur();

